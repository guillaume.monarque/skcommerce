var __create = Object.create;
var __defProp = Object.defineProperty;
var __getOwnPropDesc = Object.getOwnPropertyDescriptor;
var __getOwnPropNames = Object.getOwnPropertyNames;
var __getProtoOf = Object.getPrototypeOf;
var __hasOwnProp = Object.prototype.hasOwnProperty;
var __markAsModule = (target) => __defProp(target, "__esModule", { value: true });
var __export = (target, all) => {
  __markAsModule(target);
  for (var name in all)
    __defProp(target, name, { get: all[name], enumerable: true });
};
var __reExport = (target, module2, desc) => {
  if (module2 && typeof module2 === "object" || typeof module2 === "function") {
    for (let key of __getOwnPropNames(module2))
      if (!__hasOwnProp.call(target, key) && key !== "default")
        __defProp(target, key, { get: () => module2[key], enumerable: !(desc = __getOwnPropDesc(module2, key)) || desc.enumerable });
  }
  return target;
};
var __toModule = (module2) => {
  return __reExport(__markAsModule(__defProp(module2 != null ? __create(__getProtoOf(module2)) : {}, "default", module2 && module2.__esModule && "default" in module2 ? { get: () => module2.default, enumerable: true } : { value: module2, enumerable: true })), module2);
};

// .svelte-kit/netlify/entry.js
__export(exports, {
  handler: () => handler
});

// node_modules/@sveltejs/kit/dist/install-fetch.js
var import_http = __toModule(require("http"));
var import_https = __toModule(require("https"));
var import_zlib = __toModule(require("zlib"));
var import_stream = __toModule(require("stream"));
var import_util = __toModule(require("util"));
var import_crypto = __toModule(require("crypto"));
var import_url = __toModule(require("url"));
function dataUriToBuffer(uri) {
  if (!/^data:/i.test(uri)) {
    throw new TypeError('`uri` does not appear to be a Data URI (must begin with "data:")');
  }
  uri = uri.replace(/\r?\n/g, "");
  const firstComma = uri.indexOf(",");
  if (firstComma === -1 || firstComma <= 4) {
    throw new TypeError("malformed data: URI");
  }
  const meta = uri.substring(5, firstComma).split(";");
  let charset = "";
  let base64 = false;
  const type = meta[0] || "text/plain";
  let typeFull = type;
  for (let i = 1; i < meta.length; i++) {
    if (meta[i] === "base64") {
      base64 = true;
    } else {
      typeFull += `;${meta[i]}`;
      if (meta[i].indexOf("charset=") === 0) {
        charset = meta[i].substring(8);
      }
    }
  }
  if (!meta[0] && !charset.length) {
    typeFull += ";charset=US-ASCII";
    charset = "US-ASCII";
  }
  const encoding = base64 ? "base64" : "ascii";
  const data = unescape(uri.substring(firstComma + 1));
  const buffer = Buffer.from(data, encoding);
  buffer.type = type;
  buffer.typeFull = typeFull;
  buffer.charset = charset;
  return buffer;
}
var src = dataUriToBuffer;
var dataUriToBuffer$1 = src;
var { Readable } = import_stream.default;
var wm = new WeakMap();
async function* read(parts) {
  for (const part of parts) {
    if ("stream" in part) {
      yield* part.stream();
    } else {
      yield part;
    }
  }
}
var Blob = class {
  constructor(blobParts = [], options2 = {}) {
    let size = 0;
    const parts = blobParts.map((element) => {
      let buffer;
      if (element instanceof Buffer) {
        buffer = element;
      } else if (ArrayBuffer.isView(element)) {
        buffer = Buffer.from(element.buffer, element.byteOffset, element.byteLength);
      } else if (element instanceof ArrayBuffer) {
        buffer = Buffer.from(element);
      } else if (element instanceof Blob) {
        buffer = element;
      } else {
        buffer = Buffer.from(typeof element === "string" ? element : String(element));
      }
      size += buffer.length || buffer.size || 0;
      return buffer;
    });
    const type = options2.type === void 0 ? "" : String(options2.type).toLowerCase();
    wm.set(this, {
      type: /[^\u0020-\u007E]/.test(type) ? "" : type,
      size,
      parts
    });
  }
  get size() {
    return wm.get(this).size;
  }
  get type() {
    return wm.get(this).type;
  }
  async text() {
    return Buffer.from(await this.arrayBuffer()).toString();
  }
  async arrayBuffer() {
    const data = new Uint8Array(this.size);
    let offset = 0;
    for await (const chunk of this.stream()) {
      data.set(chunk, offset);
      offset += chunk.length;
    }
    return data.buffer;
  }
  stream() {
    return Readable.from(read(wm.get(this).parts));
  }
  slice(start = 0, end = this.size, type = "") {
    const { size } = this;
    let relativeStart = start < 0 ? Math.max(size + start, 0) : Math.min(start, size);
    let relativeEnd = end < 0 ? Math.max(size + end, 0) : Math.min(end, size);
    const span = Math.max(relativeEnd - relativeStart, 0);
    const parts = wm.get(this).parts.values();
    const blobParts = [];
    let added = 0;
    for (const part of parts) {
      const size2 = ArrayBuffer.isView(part) ? part.byteLength : part.size;
      if (relativeStart && size2 <= relativeStart) {
        relativeStart -= size2;
        relativeEnd -= size2;
      } else {
        const chunk = part.slice(relativeStart, Math.min(size2, relativeEnd));
        blobParts.push(chunk);
        added += ArrayBuffer.isView(chunk) ? chunk.byteLength : chunk.size;
        relativeStart = 0;
        if (added >= span) {
          break;
        }
      }
    }
    const blob = new Blob([], { type: String(type).toLowerCase() });
    Object.assign(wm.get(blob), { size: span, parts: blobParts });
    return blob;
  }
  get [Symbol.toStringTag]() {
    return "Blob";
  }
  static [Symbol.hasInstance](object) {
    return object && typeof object === "object" && typeof object.stream === "function" && object.stream.length === 0 && typeof object.constructor === "function" && /^(Blob|File)$/.test(object[Symbol.toStringTag]);
  }
};
Object.defineProperties(Blob.prototype, {
  size: { enumerable: true },
  type: { enumerable: true },
  slice: { enumerable: true }
});
var fetchBlob = Blob;
var Blob$1 = fetchBlob;
var FetchBaseError = class extends Error {
  constructor(message, type) {
    super(message);
    Error.captureStackTrace(this, this.constructor);
    this.type = type;
  }
  get name() {
    return this.constructor.name;
  }
  get [Symbol.toStringTag]() {
    return this.constructor.name;
  }
};
var FetchError = class extends FetchBaseError {
  constructor(message, type, systemError) {
    super(message, type);
    if (systemError) {
      this.code = this.errno = systemError.code;
      this.erroredSysCall = systemError.syscall;
    }
  }
};
var NAME = Symbol.toStringTag;
var isURLSearchParameters = (object) => {
  return typeof object === "object" && typeof object.append === "function" && typeof object.delete === "function" && typeof object.get === "function" && typeof object.getAll === "function" && typeof object.has === "function" && typeof object.set === "function" && typeof object.sort === "function" && object[NAME] === "URLSearchParams";
};
var isBlob = (object) => {
  return typeof object === "object" && typeof object.arrayBuffer === "function" && typeof object.type === "string" && typeof object.stream === "function" && typeof object.constructor === "function" && /^(Blob|File)$/.test(object[NAME]);
};
function isFormData(object) {
  return typeof object === "object" && typeof object.append === "function" && typeof object.set === "function" && typeof object.get === "function" && typeof object.getAll === "function" && typeof object.delete === "function" && typeof object.keys === "function" && typeof object.values === "function" && typeof object.entries === "function" && typeof object.constructor === "function" && object[NAME] === "FormData";
}
var isAbortSignal = (object) => {
  return typeof object === "object" && object[NAME] === "AbortSignal";
};
var carriage = "\r\n";
var dashes = "-".repeat(2);
var carriageLength = Buffer.byteLength(carriage);
var getFooter = (boundary) => `${dashes}${boundary}${dashes}${carriage.repeat(2)}`;
function getHeader(boundary, name, field) {
  let header2 = "";
  header2 += `${dashes}${boundary}${carriage}`;
  header2 += `Content-Disposition: form-data; name="${name}"`;
  if (isBlob(field)) {
    header2 += `; filename="${field.name}"${carriage}`;
    header2 += `Content-Type: ${field.type || "application/octet-stream"}`;
  }
  return `${header2}${carriage.repeat(2)}`;
}
var getBoundary = () => (0, import_crypto.randomBytes)(8).toString("hex");
async function* formDataIterator(form, boundary) {
  for (const [name, value] of form) {
    yield getHeader(boundary, name, value);
    if (isBlob(value)) {
      yield* value.stream();
    } else {
      yield value;
    }
    yield carriage;
  }
  yield getFooter(boundary);
}
function getFormDataLength(form, boundary) {
  let length = 0;
  for (const [name, value] of form) {
    length += Buffer.byteLength(getHeader(boundary, name, value));
    if (isBlob(value)) {
      length += value.size;
    } else {
      length += Buffer.byteLength(String(value));
    }
    length += carriageLength;
  }
  length += Buffer.byteLength(getFooter(boundary));
  return length;
}
var INTERNALS$2 = Symbol("Body internals");
var Body = class {
  constructor(body, {
    size = 0
  } = {}) {
    let boundary = null;
    if (body === null) {
      body = null;
    } else if (isURLSearchParameters(body)) {
      body = Buffer.from(body.toString());
    } else if (isBlob(body))
      ;
    else if (Buffer.isBuffer(body))
      ;
    else if (import_util.types.isAnyArrayBuffer(body)) {
      body = Buffer.from(body);
    } else if (ArrayBuffer.isView(body)) {
      body = Buffer.from(body.buffer, body.byteOffset, body.byteLength);
    } else if (body instanceof import_stream.default)
      ;
    else if (isFormData(body)) {
      boundary = `NodeFetchFormDataBoundary${getBoundary()}`;
      body = import_stream.default.Readable.from(formDataIterator(body, boundary));
    } else {
      body = Buffer.from(String(body));
    }
    this[INTERNALS$2] = {
      body,
      boundary,
      disturbed: false,
      error: null
    };
    this.size = size;
    if (body instanceof import_stream.default) {
      body.on("error", (err) => {
        const error3 = err instanceof FetchBaseError ? err : new FetchError(`Invalid response body while trying to fetch ${this.url}: ${err.message}`, "system", err);
        this[INTERNALS$2].error = error3;
      });
    }
  }
  get body() {
    return this[INTERNALS$2].body;
  }
  get bodyUsed() {
    return this[INTERNALS$2].disturbed;
  }
  async arrayBuffer() {
    const { buffer, byteOffset, byteLength } = await consumeBody(this);
    return buffer.slice(byteOffset, byteOffset + byteLength);
  }
  async blob() {
    const ct = this.headers && this.headers.get("content-type") || this[INTERNALS$2].body && this[INTERNALS$2].body.type || "";
    const buf = await this.buffer();
    return new Blob$1([buf], {
      type: ct
    });
  }
  async json() {
    const buffer = await consumeBody(this);
    return JSON.parse(buffer.toString());
  }
  async text() {
    const buffer = await consumeBody(this);
    return buffer.toString();
  }
  buffer() {
    return consumeBody(this);
  }
};
Object.defineProperties(Body.prototype, {
  body: { enumerable: true },
  bodyUsed: { enumerable: true },
  arrayBuffer: { enumerable: true },
  blob: { enumerable: true },
  json: { enumerable: true },
  text: { enumerable: true }
});
async function consumeBody(data) {
  if (data[INTERNALS$2].disturbed) {
    throw new TypeError(`body used already for: ${data.url}`);
  }
  data[INTERNALS$2].disturbed = true;
  if (data[INTERNALS$2].error) {
    throw data[INTERNALS$2].error;
  }
  let { body } = data;
  if (body === null) {
    return Buffer.alloc(0);
  }
  if (isBlob(body)) {
    body = body.stream();
  }
  if (Buffer.isBuffer(body)) {
    return body;
  }
  if (!(body instanceof import_stream.default)) {
    return Buffer.alloc(0);
  }
  const accum = [];
  let accumBytes = 0;
  try {
    for await (const chunk of body) {
      if (data.size > 0 && accumBytes + chunk.length > data.size) {
        const err = new FetchError(`content size at ${data.url} over limit: ${data.size}`, "max-size");
        body.destroy(err);
        throw err;
      }
      accumBytes += chunk.length;
      accum.push(chunk);
    }
  } catch (error3) {
    if (error3 instanceof FetchBaseError) {
      throw error3;
    } else {
      throw new FetchError(`Invalid response body while trying to fetch ${data.url}: ${error3.message}`, "system", error3);
    }
  }
  if (body.readableEnded === true || body._readableState.ended === true) {
    try {
      if (accum.every((c) => typeof c === "string")) {
        return Buffer.from(accum.join(""));
      }
      return Buffer.concat(accum, accumBytes);
    } catch (error3) {
      throw new FetchError(`Could not create Buffer from response body for ${data.url}: ${error3.message}`, "system", error3);
    }
  } else {
    throw new FetchError(`Premature close of server response while trying to fetch ${data.url}`);
  }
}
var clone = (instance, highWaterMark) => {
  let p1;
  let p2;
  let { body } = instance;
  if (instance.bodyUsed) {
    throw new Error("cannot clone body after it is used");
  }
  if (body instanceof import_stream.default && typeof body.getBoundary !== "function") {
    p1 = new import_stream.PassThrough({ highWaterMark });
    p2 = new import_stream.PassThrough({ highWaterMark });
    body.pipe(p1);
    body.pipe(p2);
    instance[INTERNALS$2].body = p1;
    body = p2;
  }
  return body;
};
var extractContentType = (body, request) => {
  if (body === null) {
    return null;
  }
  if (typeof body === "string") {
    return "text/plain;charset=UTF-8";
  }
  if (isURLSearchParameters(body)) {
    return "application/x-www-form-urlencoded;charset=UTF-8";
  }
  if (isBlob(body)) {
    return body.type || null;
  }
  if (Buffer.isBuffer(body) || import_util.types.isAnyArrayBuffer(body) || ArrayBuffer.isView(body)) {
    return null;
  }
  if (body && typeof body.getBoundary === "function") {
    return `multipart/form-data;boundary=${body.getBoundary()}`;
  }
  if (isFormData(body)) {
    return `multipart/form-data; boundary=${request[INTERNALS$2].boundary}`;
  }
  if (body instanceof import_stream.default) {
    return null;
  }
  return "text/plain;charset=UTF-8";
};
var getTotalBytes = (request) => {
  const { body } = request;
  if (body === null) {
    return 0;
  }
  if (isBlob(body)) {
    return body.size;
  }
  if (Buffer.isBuffer(body)) {
    return body.length;
  }
  if (body && typeof body.getLengthSync === "function") {
    return body.hasKnownLength && body.hasKnownLength() ? body.getLengthSync() : null;
  }
  if (isFormData(body)) {
    return getFormDataLength(request[INTERNALS$2].boundary);
  }
  return null;
};
var writeToStream = (dest, { body }) => {
  if (body === null) {
    dest.end();
  } else if (isBlob(body)) {
    body.stream().pipe(dest);
  } else if (Buffer.isBuffer(body)) {
    dest.write(body);
    dest.end();
  } else {
    body.pipe(dest);
  }
};
var validateHeaderName = typeof import_http.default.validateHeaderName === "function" ? import_http.default.validateHeaderName : (name) => {
  if (!/^[\^`\-\w!#$%&'*+.|~]+$/.test(name)) {
    const err = new TypeError(`Header name must be a valid HTTP token [${name}]`);
    Object.defineProperty(err, "code", { value: "ERR_INVALID_HTTP_TOKEN" });
    throw err;
  }
};
var validateHeaderValue = typeof import_http.default.validateHeaderValue === "function" ? import_http.default.validateHeaderValue : (name, value) => {
  if (/[^\t\u0020-\u007E\u0080-\u00FF]/.test(value)) {
    const err = new TypeError(`Invalid character in header content ["${name}"]`);
    Object.defineProperty(err, "code", { value: "ERR_INVALID_CHAR" });
    throw err;
  }
};
var Headers = class extends URLSearchParams {
  constructor(init2) {
    let result = [];
    if (init2 instanceof Headers) {
      const raw = init2.raw();
      for (const [name, values] of Object.entries(raw)) {
        result.push(...values.map((value) => [name, value]));
      }
    } else if (init2 == null)
      ;
    else if (typeof init2 === "object" && !import_util.types.isBoxedPrimitive(init2)) {
      const method = init2[Symbol.iterator];
      if (method == null) {
        result.push(...Object.entries(init2));
      } else {
        if (typeof method !== "function") {
          throw new TypeError("Header pairs must be iterable");
        }
        result = [...init2].map((pair) => {
          if (typeof pair !== "object" || import_util.types.isBoxedPrimitive(pair)) {
            throw new TypeError("Each header pair must be an iterable object");
          }
          return [...pair];
        }).map((pair) => {
          if (pair.length !== 2) {
            throw new TypeError("Each header pair must be a name/value tuple");
          }
          return [...pair];
        });
      }
    } else {
      throw new TypeError("Failed to construct 'Headers': The provided value is not of type '(sequence<sequence<ByteString>> or record<ByteString, ByteString>)");
    }
    result = result.length > 0 ? result.map(([name, value]) => {
      validateHeaderName(name);
      validateHeaderValue(name, String(value));
      return [String(name).toLowerCase(), String(value)];
    }) : void 0;
    super(result);
    return new Proxy(this, {
      get(target, p, receiver) {
        switch (p) {
          case "append":
          case "set":
            return (name, value) => {
              validateHeaderName(name);
              validateHeaderValue(name, String(value));
              return URLSearchParams.prototype[p].call(receiver, String(name).toLowerCase(), String(value));
            };
          case "delete":
          case "has":
          case "getAll":
            return (name) => {
              validateHeaderName(name);
              return URLSearchParams.prototype[p].call(receiver, String(name).toLowerCase());
            };
          case "keys":
            return () => {
              target.sort();
              return new Set(URLSearchParams.prototype.keys.call(target)).keys();
            };
          default:
            return Reflect.get(target, p, receiver);
        }
      }
    });
  }
  get [Symbol.toStringTag]() {
    return this.constructor.name;
  }
  toString() {
    return Object.prototype.toString.call(this);
  }
  get(name) {
    const values = this.getAll(name);
    if (values.length === 0) {
      return null;
    }
    let value = values.join(", ");
    if (/^content-encoding$/i.test(name)) {
      value = value.toLowerCase();
    }
    return value;
  }
  forEach(callback) {
    for (const name of this.keys()) {
      callback(this.get(name), name);
    }
  }
  *values() {
    for (const name of this.keys()) {
      yield this.get(name);
    }
  }
  *entries() {
    for (const name of this.keys()) {
      yield [name, this.get(name)];
    }
  }
  [Symbol.iterator]() {
    return this.entries();
  }
  raw() {
    return [...this.keys()].reduce((result, key) => {
      result[key] = this.getAll(key);
      return result;
    }, {});
  }
  [Symbol.for("nodejs.util.inspect.custom")]() {
    return [...this.keys()].reduce((result, key) => {
      const values = this.getAll(key);
      if (key === "host") {
        result[key] = values[0];
      } else {
        result[key] = values.length > 1 ? values : values[0];
      }
      return result;
    }, {});
  }
};
Object.defineProperties(Headers.prototype, ["get", "entries", "forEach", "values"].reduce((result, property) => {
  result[property] = { enumerable: true };
  return result;
}, {}));
function fromRawHeaders(headers = []) {
  return new Headers(headers.reduce((result, value, index2, array) => {
    if (index2 % 2 === 0) {
      result.push(array.slice(index2, index2 + 2));
    }
    return result;
  }, []).filter(([name, value]) => {
    try {
      validateHeaderName(name);
      validateHeaderValue(name, String(value));
      return true;
    } catch {
      return false;
    }
  }));
}
var redirectStatus = new Set([301, 302, 303, 307, 308]);
var isRedirect = (code) => {
  return redirectStatus.has(code);
};
var INTERNALS$1 = Symbol("Response internals");
var Response = class extends Body {
  constructor(body = null, options2 = {}) {
    super(body, options2);
    const status = options2.status || 200;
    const headers = new Headers(options2.headers);
    if (body !== null && !headers.has("Content-Type")) {
      const contentType = extractContentType(body);
      if (contentType) {
        headers.append("Content-Type", contentType);
      }
    }
    this[INTERNALS$1] = {
      url: options2.url,
      status,
      statusText: options2.statusText || "",
      headers,
      counter: options2.counter,
      highWaterMark: options2.highWaterMark
    };
  }
  get url() {
    return this[INTERNALS$1].url || "";
  }
  get status() {
    return this[INTERNALS$1].status;
  }
  get ok() {
    return this[INTERNALS$1].status >= 200 && this[INTERNALS$1].status < 300;
  }
  get redirected() {
    return this[INTERNALS$1].counter > 0;
  }
  get statusText() {
    return this[INTERNALS$1].statusText;
  }
  get headers() {
    return this[INTERNALS$1].headers;
  }
  get highWaterMark() {
    return this[INTERNALS$1].highWaterMark;
  }
  clone() {
    return new Response(clone(this, this.highWaterMark), {
      url: this.url,
      status: this.status,
      statusText: this.statusText,
      headers: this.headers,
      ok: this.ok,
      redirected: this.redirected,
      size: this.size
    });
  }
  static redirect(url, status = 302) {
    if (!isRedirect(status)) {
      throw new RangeError('Failed to execute "redirect" on "response": Invalid status code');
    }
    return new Response(null, {
      headers: {
        location: new URL(url).toString()
      },
      status
    });
  }
  get [Symbol.toStringTag]() {
    return "Response";
  }
};
Object.defineProperties(Response.prototype, {
  url: { enumerable: true },
  status: { enumerable: true },
  ok: { enumerable: true },
  redirected: { enumerable: true },
  statusText: { enumerable: true },
  headers: { enumerable: true },
  clone: { enumerable: true }
});
var getSearch = (parsedURL) => {
  if (parsedURL.search) {
    return parsedURL.search;
  }
  const lastOffset = parsedURL.href.length - 1;
  const hash2 = parsedURL.hash || (parsedURL.href[lastOffset] === "#" ? "#" : "");
  return parsedURL.href[lastOffset - hash2.length] === "?" ? "?" : "";
};
var INTERNALS = Symbol("Request internals");
var isRequest = (object) => {
  return typeof object === "object" && typeof object[INTERNALS] === "object";
};
var Request = class extends Body {
  constructor(input, init2 = {}) {
    let parsedURL;
    if (isRequest(input)) {
      parsedURL = new URL(input.url);
    } else {
      parsedURL = new URL(input);
      input = {};
    }
    let method = init2.method || input.method || "GET";
    method = method.toUpperCase();
    if ((init2.body != null || isRequest(input)) && input.body !== null && (method === "GET" || method === "HEAD")) {
      throw new TypeError("Request with GET/HEAD method cannot have body");
    }
    const inputBody = init2.body ? init2.body : isRequest(input) && input.body !== null ? clone(input) : null;
    super(inputBody, {
      size: init2.size || input.size || 0
    });
    const headers = new Headers(init2.headers || input.headers || {});
    if (inputBody !== null && !headers.has("Content-Type")) {
      const contentType = extractContentType(inputBody, this);
      if (contentType) {
        headers.append("Content-Type", contentType);
      }
    }
    let signal = isRequest(input) ? input.signal : null;
    if ("signal" in init2) {
      signal = init2.signal;
    }
    if (signal !== null && !isAbortSignal(signal)) {
      throw new TypeError("Expected signal to be an instanceof AbortSignal");
    }
    this[INTERNALS] = {
      method,
      redirect: init2.redirect || input.redirect || "follow",
      headers,
      parsedURL,
      signal
    };
    this.follow = init2.follow === void 0 ? input.follow === void 0 ? 20 : input.follow : init2.follow;
    this.compress = init2.compress === void 0 ? input.compress === void 0 ? true : input.compress : init2.compress;
    this.counter = init2.counter || input.counter || 0;
    this.agent = init2.agent || input.agent;
    this.highWaterMark = init2.highWaterMark || input.highWaterMark || 16384;
    this.insecureHTTPParser = init2.insecureHTTPParser || input.insecureHTTPParser || false;
  }
  get method() {
    return this[INTERNALS].method;
  }
  get url() {
    return (0, import_url.format)(this[INTERNALS].parsedURL);
  }
  get headers() {
    return this[INTERNALS].headers;
  }
  get redirect() {
    return this[INTERNALS].redirect;
  }
  get signal() {
    return this[INTERNALS].signal;
  }
  clone() {
    return new Request(this);
  }
  get [Symbol.toStringTag]() {
    return "Request";
  }
};
Object.defineProperties(Request.prototype, {
  method: { enumerable: true },
  url: { enumerable: true },
  headers: { enumerable: true },
  redirect: { enumerable: true },
  clone: { enumerable: true },
  signal: { enumerable: true }
});
var getNodeRequestOptions = (request) => {
  const { parsedURL } = request[INTERNALS];
  const headers = new Headers(request[INTERNALS].headers);
  if (!headers.has("Accept")) {
    headers.set("Accept", "*/*");
  }
  let contentLengthValue = null;
  if (request.body === null && /^(post|put)$/i.test(request.method)) {
    contentLengthValue = "0";
  }
  if (request.body !== null) {
    const totalBytes = getTotalBytes(request);
    if (typeof totalBytes === "number" && !Number.isNaN(totalBytes)) {
      contentLengthValue = String(totalBytes);
    }
  }
  if (contentLengthValue) {
    headers.set("Content-Length", contentLengthValue);
  }
  if (!headers.has("User-Agent")) {
    headers.set("User-Agent", "node-fetch");
  }
  if (request.compress && !headers.has("Accept-Encoding")) {
    headers.set("Accept-Encoding", "gzip,deflate,br");
  }
  let { agent } = request;
  if (typeof agent === "function") {
    agent = agent(parsedURL);
  }
  if (!headers.has("Connection") && !agent) {
    headers.set("Connection", "close");
  }
  const search = getSearch(parsedURL);
  const requestOptions = {
    path: parsedURL.pathname + search,
    pathname: parsedURL.pathname,
    hostname: parsedURL.hostname,
    protocol: parsedURL.protocol,
    port: parsedURL.port,
    hash: parsedURL.hash,
    search: parsedURL.search,
    query: parsedURL.query,
    href: parsedURL.href,
    method: request.method,
    headers: headers[Symbol.for("nodejs.util.inspect.custom")](),
    insecureHTTPParser: request.insecureHTTPParser,
    agent
  };
  return requestOptions;
};
var AbortError = class extends FetchBaseError {
  constructor(message, type = "aborted") {
    super(message, type);
  }
};
var supportedSchemas = new Set(["data:", "http:", "https:"]);
async function fetch(url, options_) {
  return new Promise((resolve2, reject) => {
    const request = new Request(url, options_);
    const options2 = getNodeRequestOptions(request);
    if (!supportedSchemas.has(options2.protocol)) {
      throw new TypeError(`node-fetch cannot load ${url}. URL scheme "${options2.protocol.replace(/:$/, "")}" is not supported.`);
    }
    if (options2.protocol === "data:") {
      const data = dataUriToBuffer$1(request.url);
      const response2 = new Response(data, { headers: { "Content-Type": data.typeFull } });
      resolve2(response2);
      return;
    }
    const send = (options2.protocol === "https:" ? import_https.default : import_http.default).request;
    const { signal } = request;
    let response = null;
    const abort = () => {
      const error3 = new AbortError("The operation was aborted.");
      reject(error3);
      if (request.body && request.body instanceof import_stream.default.Readable) {
        request.body.destroy(error3);
      }
      if (!response || !response.body) {
        return;
      }
      response.body.emit("error", error3);
    };
    if (signal && signal.aborted) {
      abort();
      return;
    }
    const abortAndFinalize = () => {
      abort();
      finalize();
    };
    const request_ = send(options2);
    if (signal) {
      signal.addEventListener("abort", abortAndFinalize);
    }
    const finalize = () => {
      request_.abort();
      if (signal) {
        signal.removeEventListener("abort", abortAndFinalize);
      }
    };
    request_.on("error", (err) => {
      reject(new FetchError(`request to ${request.url} failed, reason: ${err.message}`, "system", err));
      finalize();
    });
    request_.on("response", (response_) => {
      request_.setTimeout(0);
      const headers = fromRawHeaders(response_.rawHeaders);
      if (isRedirect(response_.statusCode)) {
        const location = headers.get("Location");
        const locationURL = location === null ? null : new URL(location, request.url);
        switch (request.redirect) {
          case "error":
            reject(new FetchError(`uri requested responds with a redirect, redirect mode is set to error: ${request.url}`, "no-redirect"));
            finalize();
            return;
          case "manual":
            if (locationURL !== null) {
              try {
                headers.set("Location", locationURL);
              } catch (error3) {
                reject(error3);
              }
            }
            break;
          case "follow": {
            if (locationURL === null) {
              break;
            }
            if (request.counter >= request.follow) {
              reject(new FetchError(`maximum redirect reached at: ${request.url}`, "max-redirect"));
              finalize();
              return;
            }
            const requestOptions = {
              headers: new Headers(request.headers),
              follow: request.follow,
              counter: request.counter + 1,
              agent: request.agent,
              compress: request.compress,
              method: request.method,
              body: request.body,
              signal: request.signal,
              size: request.size
            };
            if (response_.statusCode !== 303 && request.body && options_.body instanceof import_stream.default.Readable) {
              reject(new FetchError("Cannot follow redirect with body being a readable stream", "unsupported-redirect"));
              finalize();
              return;
            }
            if (response_.statusCode === 303 || (response_.statusCode === 301 || response_.statusCode === 302) && request.method === "POST") {
              requestOptions.method = "GET";
              requestOptions.body = void 0;
              requestOptions.headers.delete("content-length");
            }
            resolve2(fetch(new Request(locationURL, requestOptions)));
            finalize();
            return;
          }
        }
      }
      response_.once("end", () => {
        if (signal) {
          signal.removeEventListener("abort", abortAndFinalize);
        }
      });
      let body = (0, import_stream.pipeline)(response_, new import_stream.PassThrough(), (error3) => {
        reject(error3);
      });
      if (process.version < "v12.10") {
        response_.on("aborted", abortAndFinalize);
      }
      const responseOptions = {
        url: request.url,
        status: response_.statusCode,
        statusText: response_.statusMessage,
        headers,
        size: request.size,
        counter: request.counter,
        highWaterMark: request.highWaterMark
      };
      const codings = headers.get("Content-Encoding");
      if (!request.compress || request.method === "HEAD" || codings === null || response_.statusCode === 204 || response_.statusCode === 304) {
        response = new Response(body, responseOptions);
        resolve2(response);
        return;
      }
      const zlibOptions = {
        flush: import_zlib.default.Z_SYNC_FLUSH,
        finishFlush: import_zlib.default.Z_SYNC_FLUSH
      };
      if (codings === "gzip" || codings === "x-gzip") {
        body = (0, import_stream.pipeline)(body, import_zlib.default.createGunzip(zlibOptions), (error3) => {
          reject(error3);
        });
        response = new Response(body, responseOptions);
        resolve2(response);
        return;
      }
      if (codings === "deflate" || codings === "x-deflate") {
        const raw = (0, import_stream.pipeline)(response_, new import_stream.PassThrough(), (error3) => {
          reject(error3);
        });
        raw.once("data", (chunk) => {
          if ((chunk[0] & 15) === 8) {
            body = (0, import_stream.pipeline)(body, import_zlib.default.createInflate(), (error3) => {
              reject(error3);
            });
          } else {
            body = (0, import_stream.pipeline)(body, import_zlib.default.createInflateRaw(), (error3) => {
              reject(error3);
            });
          }
          response = new Response(body, responseOptions);
          resolve2(response);
        });
        return;
      }
      if (codings === "br") {
        body = (0, import_stream.pipeline)(body, import_zlib.default.createBrotliDecompress(), (error3) => {
          reject(error3);
        });
        response = new Response(body, responseOptions);
        resolve2(response);
        return;
      }
      response = new Response(body, responseOptions);
      resolve2(response);
    });
    writeToStream(request_, request);
  });
}

// node_modules/@sveltejs/kit/dist/ssr.js
function lowercase_keys(obj) {
  const clone2 = {};
  for (const key in obj) {
    clone2[key.toLowerCase()] = obj[key];
  }
  return clone2;
}
function error(body) {
  return {
    status: 500,
    body,
    headers: {}
  };
}
function is_string(s2) {
  return typeof s2 === "string" || s2 instanceof String;
}
function is_content_type_textual(content_type) {
  if (!content_type)
    return true;
  const [type] = content_type.split(";");
  return type === "text/plain" || type === "application/json" || type === "application/x-www-form-urlencoded" || type === "multipart/form-data";
}
async function render_endpoint(request, route, match) {
  const mod = await route.load();
  const handler2 = mod[request.method.toLowerCase().replace("delete", "del")];
  if (!handler2) {
    return;
  }
  const params = route.params(match);
  const response = await handler2({ ...request, params });
  const preface = `Invalid response from route ${request.path}`;
  if (!response) {
    return;
  }
  if (typeof response !== "object") {
    return error(`${preface}: expected an object, got ${typeof response}`);
  }
  let { status = 200, body, headers = {} } = response;
  headers = lowercase_keys(headers);
  const type = headers["content-type"];
  const is_type_textual = is_content_type_textual(type);
  if (!is_type_textual && !(body instanceof Uint8Array || is_string(body))) {
    return error(`${preface}: body must be an instance of string or Uint8Array if content-type is not a supported textual content-type`);
  }
  let normalized_body;
  if ((typeof body === "object" || typeof body === "undefined") && !(body instanceof Uint8Array) && (!type || type.startsWith("application/json"))) {
    headers = { ...headers, "content-type": "application/json; charset=utf-8" };
    normalized_body = JSON.stringify(typeof body === "undefined" ? {} : body);
  } else {
    normalized_body = body;
  }
  return { status, body: normalized_body, headers };
}
var chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_$";
var unsafeChars = /[<>\b\f\n\r\t\0\u2028\u2029]/g;
var reserved = /^(?:do|if|in|for|int|let|new|try|var|byte|case|char|else|enum|goto|long|this|void|with|await|break|catch|class|const|final|float|short|super|throw|while|yield|delete|double|export|import|native|return|switch|throws|typeof|boolean|default|extends|finally|package|private|abstract|continue|debugger|function|volatile|interface|protected|transient|implements|instanceof|synchronized)$/;
var escaped$1 = {
  "<": "\\u003C",
  ">": "\\u003E",
  "/": "\\u002F",
  "\\": "\\\\",
  "\b": "\\b",
  "\f": "\\f",
  "\n": "\\n",
  "\r": "\\r",
  "	": "\\t",
  "\0": "\\0",
  "\u2028": "\\u2028",
  "\u2029": "\\u2029"
};
var objectProtoOwnPropertyNames = Object.getOwnPropertyNames(Object.prototype).sort().join("\0");
function devalue(value) {
  var counts = new Map();
  function walk(thing) {
    if (typeof thing === "function") {
      throw new Error("Cannot stringify a function");
    }
    if (counts.has(thing)) {
      counts.set(thing, counts.get(thing) + 1);
      return;
    }
    counts.set(thing, 1);
    if (!isPrimitive(thing)) {
      var type = getType(thing);
      switch (type) {
        case "Number":
        case "String":
        case "Boolean":
        case "Date":
        case "RegExp":
          return;
        case "Array":
          thing.forEach(walk);
          break;
        case "Set":
        case "Map":
          Array.from(thing).forEach(walk);
          break;
        default:
          var proto = Object.getPrototypeOf(thing);
          if (proto !== Object.prototype && proto !== null && Object.getOwnPropertyNames(proto).sort().join("\0") !== objectProtoOwnPropertyNames) {
            throw new Error("Cannot stringify arbitrary non-POJOs");
          }
          if (Object.getOwnPropertySymbols(thing).length > 0) {
            throw new Error("Cannot stringify POJOs with symbolic keys");
          }
          Object.keys(thing).forEach(function(key) {
            return walk(thing[key]);
          });
      }
    }
  }
  walk(value);
  var names = new Map();
  Array.from(counts).filter(function(entry) {
    return entry[1] > 1;
  }).sort(function(a, b) {
    return b[1] - a[1];
  }).forEach(function(entry, i) {
    names.set(entry[0], getName(i));
  });
  function stringify(thing) {
    if (names.has(thing)) {
      return names.get(thing);
    }
    if (isPrimitive(thing)) {
      return stringifyPrimitive(thing);
    }
    var type = getType(thing);
    switch (type) {
      case "Number":
      case "String":
      case "Boolean":
        return "Object(" + stringify(thing.valueOf()) + ")";
      case "RegExp":
        return "new RegExp(" + stringifyString(thing.source) + ', "' + thing.flags + '")';
      case "Date":
        return "new Date(" + thing.getTime() + ")";
      case "Array":
        var members = thing.map(function(v, i) {
          return i in thing ? stringify(v) : "";
        });
        var tail = thing.length === 0 || thing.length - 1 in thing ? "" : ",";
        return "[" + members.join(",") + tail + "]";
      case "Set":
      case "Map":
        return "new " + type + "([" + Array.from(thing).map(stringify).join(",") + "])";
      default:
        var obj = "{" + Object.keys(thing).map(function(key) {
          return safeKey(key) + ":" + stringify(thing[key]);
        }).join(",") + "}";
        var proto = Object.getPrototypeOf(thing);
        if (proto === null) {
          return Object.keys(thing).length > 0 ? "Object.assign(Object.create(null)," + obj + ")" : "Object.create(null)";
        }
        return obj;
    }
  }
  var str = stringify(value);
  if (names.size) {
    var params_1 = [];
    var statements_1 = [];
    var values_1 = [];
    names.forEach(function(name, thing) {
      params_1.push(name);
      if (isPrimitive(thing)) {
        values_1.push(stringifyPrimitive(thing));
        return;
      }
      var type = getType(thing);
      switch (type) {
        case "Number":
        case "String":
        case "Boolean":
          values_1.push("Object(" + stringify(thing.valueOf()) + ")");
          break;
        case "RegExp":
          values_1.push(thing.toString());
          break;
        case "Date":
          values_1.push("new Date(" + thing.getTime() + ")");
          break;
        case "Array":
          values_1.push("Array(" + thing.length + ")");
          thing.forEach(function(v, i) {
            statements_1.push(name + "[" + i + "]=" + stringify(v));
          });
          break;
        case "Set":
          values_1.push("new Set");
          statements_1.push(name + "." + Array.from(thing).map(function(v) {
            return "add(" + stringify(v) + ")";
          }).join("."));
          break;
        case "Map":
          values_1.push("new Map");
          statements_1.push(name + "." + Array.from(thing).map(function(_a) {
            var k = _a[0], v = _a[1];
            return "set(" + stringify(k) + ", " + stringify(v) + ")";
          }).join("."));
          break;
        default:
          values_1.push(Object.getPrototypeOf(thing) === null ? "Object.create(null)" : "{}");
          Object.keys(thing).forEach(function(key) {
            statements_1.push("" + name + safeProp(key) + "=" + stringify(thing[key]));
          });
      }
    });
    statements_1.push("return " + str);
    return "(function(" + params_1.join(",") + "){" + statements_1.join(";") + "}(" + values_1.join(",") + "))";
  } else {
    return str;
  }
}
function getName(num) {
  var name = "";
  do {
    name = chars[num % chars.length] + name;
    num = ~~(num / chars.length) - 1;
  } while (num >= 0);
  return reserved.test(name) ? name + "_" : name;
}
function isPrimitive(thing) {
  return Object(thing) !== thing;
}
function stringifyPrimitive(thing) {
  if (typeof thing === "string")
    return stringifyString(thing);
  if (thing === void 0)
    return "void 0";
  if (thing === 0 && 1 / thing < 0)
    return "-0";
  var str = String(thing);
  if (typeof thing === "number")
    return str.replace(/^(-)?0\./, "$1.");
  return str;
}
function getType(thing) {
  return Object.prototype.toString.call(thing).slice(8, -1);
}
function escapeUnsafeChar(c) {
  return escaped$1[c] || c;
}
function escapeUnsafeChars(str) {
  return str.replace(unsafeChars, escapeUnsafeChar);
}
function safeKey(key) {
  return /^[_$a-zA-Z][_$a-zA-Z0-9]*$/.test(key) ? key : escapeUnsafeChars(JSON.stringify(key));
}
function safeProp(key) {
  return /^[_$a-zA-Z][_$a-zA-Z0-9]*$/.test(key) ? "." + key : "[" + escapeUnsafeChars(JSON.stringify(key)) + "]";
}
function stringifyString(str) {
  var result = '"';
  for (var i = 0; i < str.length; i += 1) {
    var char = str.charAt(i);
    var code = char.charCodeAt(0);
    if (char === '"') {
      result += '\\"';
    } else if (char in escaped$1) {
      result += escaped$1[char];
    } else if (code >= 55296 && code <= 57343) {
      var next = str.charCodeAt(i + 1);
      if (code <= 56319 && (next >= 56320 && next <= 57343)) {
        result += char + str[++i];
      } else {
        result += "\\u" + code.toString(16).toUpperCase();
      }
    } else {
      result += char;
    }
  }
  result += '"';
  return result;
}
function noop() {
}
function safe_not_equal(a, b) {
  return a != a ? b == b : a !== b || (a && typeof a === "object" || typeof a === "function");
}
Promise.resolve();
var subscriber_queue = [];
function writable(value, start = noop) {
  let stop;
  const subscribers = [];
  function set(new_value) {
    if (safe_not_equal(value, new_value)) {
      value = new_value;
      if (stop) {
        const run_queue = !subscriber_queue.length;
        for (let i = 0; i < subscribers.length; i += 1) {
          const s2 = subscribers[i];
          s2[1]();
          subscriber_queue.push(s2, value);
        }
        if (run_queue) {
          for (let i = 0; i < subscriber_queue.length; i += 2) {
            subscriber_queue[i][0](subscriber_queue[i + 1]);
          }
          subscriber_queue.length = 0;
        }
      }
    }
  }
  function update(fn) {
    set(fn(value));
  }
  function subscribe(run2, invalidate = noop) {
    const subscriber = [run2, invalidate];
    subscribers.push(subscriber);
    if (subscribers.length === 1) {
      stop = start(set) || noop;
    }
    run2(value);
    return () => {
      const index2 = subscribers.indexOf(subscriber);
      if (index2 !== -1) {
        subscribers.splice(index2, 1);
      }
      if (subscribers.length === 0) {
        stop();
        stop = null;
      }
    };
  }
  return { set, update, subscribe };
}
function hash(value) {
  let hash2 = 5381;
  let i = value.length;
  if (typeof value === "string") {
    while (i)
      hash2 = hash2 * 33 ^ value.charCodeAt(--i);
  } else {
    while (i)
      hash2 = hash2 * 33 ^ value[--i];
  }
  return (hash2 >>> 0).toString(36);
}
var s$1 = JSON.stringify;
async function render_response({
  branch,
  options: options2,
  $session,
  page_config,
  status,
  error: error3,
  page
}) {
  const css2 = new Set(options2.entry.css);
  const js = new Set(options2.entry.js);
  const styles = new Set();
  const serialized_data = [];
  let rendered;
  let is_private = false;
  let maxage;
  if (error3) {
    error3.stack = options2.get_stack(error3);
  }
  if (page_config.ssr) {
    branch.forEach(({ node, loaded, fetched, uses_credentials }) => {
      if (node.css)
        node.css.forEach((url) => css2.add(url));
      if (node.js)
        node.js.forEach((url) => js.add(url));
      if (node.styles)
        node.styles.forEach((content) => styles.add(content));
      if (fetched && page_config.hydrate)
        serialized_data.push(...fetched);
      if (uses_credentials)
        is_private = true;
      maxage = loaded.maxage;
    });
    const session = writable($session);
    const props = {
      stores: {
        page: writable(null),
        navigating: writable(null),
        session
      },
      page,
      components: branch.map(({ node }) => node.module.default)
    };
    for (let i = 0; i < branch.length; i += 1) {
      props[`props_${i}`] = await branch[i].loaded.props;
    }
    let session_tracking_active = false;
    const unsubscribe = session.subscribe(() => {
      if (session_tracking_active)
        is_private = true;
    });
    session_tracking_active = true;
    try {
      rendered = options2.root.render(props);
    } finally {
      unsubscribe();
    }
  } else {
    rendered = { head: "", html: "", css: { code: "", map: null } };
  }
  const include_js = page_config.router || page_config.hydrate;
  if (!include_js)
    js.clear();
  const links = options2.amp ? styles.size > 0 || rendered.css.code.length > 0 ? `<style amp-custom>${Array.from(styles).concat(rendered.css.code).join("\n")}</style>` : "" : [
    ...Array.from(js).map((dep) => `<link rel="modulepreload" href="${dep}">`),
    ...Array.from(css2).map((dep) => `<link rel="stylesheet" href="${dep}">`)
  ].join("\n		");
  let init2 = "";
  if (options2.amp) {
    init2 = `
		<style amp-boilerplate>body{-webkit-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-moz-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-ms-animation:-amp-start 8s steps(1,end) 0s 1 normal both;animation:-amp-start 8s steps(1,end) 0s 1 normal both}@-webkit-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-moz-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-ms-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-o-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}</style>
		<noscript><style amp-boilerplate>body{-webkit-animation:none;-moz-animation:none;-ms-animation:none;animation:none}</style></noscript>
		<script async src="https://cdn.ampproject.org/v0.js"><\/script>`;
  } else if (include_js) {
    init2 = `<script type="module">
			import { start } from ${s$1(options2.entry.file)};
			start({
				target: ${options2.target ? `document.querySelector(${s$1(options2.target)})` : "document.body"},
				paths: ${s$1(options2.paths)},
				session: ${try_serialize($session, (error4) => {
      throw new Error(`Failed to serialize session data: ${error4.message}`);
    })},
				host: ${page && page.host ? s$1(page.host) : "location.host"},
				route: ${!!page_config.router},
				spa: ${!page_config.ssr},
				trailing_slash: ${s$1(options2.trailing_slash)},
				hydrate: ${page_config.ssr && page_config.hydrate ? `{
					status: ${status},
					error: ${serialize_error(error3)},
					nodes: [
						${(branch || []).map(({ node }) => `import(${s$1(node.entry)})`).join(",\n						")}
					],
					page: {
						host: ${page && page.host ? s$1(page.host) : "location.host"}, // TODO this is redundant
						path: ${s$1(page && page.path)},
						query: new URLSearchParams(${page ? s$1(page.query.toString()) : ""}),
						params: ${page && s$1(page.params)}
					}
				}` : "null"}
			});
		<\/script>`;
  }
  if (options2.service_worker) {
    init2 += `<script>
			if ('serviceWorker' in navigator) {
				navigator.serviceWorker.register('${options2.service_worker}');
			}
		<\/script>`;
  }
  const head = [
    rendered.head,
    styles.size && !options2.amp ? `<style data-svelte>${Array.from(styles).join("\n")}</style>` : "",
    links,
    init2
  ].join("\n\n		");
  const body = options2.amp ? rendered.html : `${rendered.html}

			${serialized_data.map(({ url, body: body2, json }) => {
    let attributes = `type="application/json" data-type="svelte-data" data-url="${url}"`;
    if (body2)
      attributes += ` data-body="${hash(body2)}"`;
    return `<script ${attributes}>${json}<\/script>`;
  }).join("\n\n			")}
		`.replace(/^\t{2}/gm, "");
  const headers = {
    "content-type": "text/html"
  };
  if (maxage) {
    headers["cache-control"] = `${is_private ? "private" : "public"}, max-age=${maxage}`;
  }
  if (!options2.floc) {
    headers["permissions-policy"] = "interest-cohort=()";
  }
  return {
    status,
    headers,
    body: options2.template({ head, body })
  };
}
function try_serialize(data, fail) {
  try {
    return devalue(data);
  } catch (err) {
    if (fail)
      fail(err);
    return null;
  }
}
function serialize_error(error3) {
  if (!error3)
    return null;
  let serialized = try_serialize(error3);
  if (!serialized) {
    const { name, message, stack } = error3;
    serialized = try_serialize({ ...error3, name, message, stack });
  }
  if (!serialized) {
    serialized = "{}";
  }
  return serialized;
}
function normalize(loaded) {
  const has_error_status = loaded.status && loaded.status >= 400 && loaded.status <= 599 && !loaded.redirect;
  if (loaded.error || has_error_status) {
    const status = loaded.status;
    if (!loaded.error && has_error_status) {
      return {
        status: status || 500,
        error: new Error()
      };
    }
    const error3 = typeof loaded.error === "string" ? new Error(loaded.error) : loaded.error;
    if (!(error3 instanceof Error)) {
      return {
        status: 500,
        error: new Error(`"error" property returned from load() must be a string or instance of Error, received type "${typeof error3}"`)
      };
    }
    if (!status || status < 400 || status > 599) {
      console.warn('"error" returned from load() without a valid status code \u2014 defaulting to 500');
      return { status: 500, error: error3 };
    }
    return { status, error: error3 };
  }
  if (loaded.redirect) {
    if (!loaded.status || Math.floor(loaded.status / 100) !== 3) {
      return {
        status: 500,
        error: new Error('"redirect" property returned from load() must be accompanied by a 3xx status code')
      };
    }
    if (typeof loaded.redirect !== "string") {
      return {
        status: 500,
        error: new Error('"redirect" property returned from load() must be a string')
      };
    }
  }
  return loaded;
}
var s = JSON.stringify;
async function load_node({
  request,
  options: options2,
  state,
  route,
  page,
  node,
  $session,
  context,
  prerender_enabled,
  is_leaf,
  is_error,
  status,
  error: error3
}) {
  const { module: module2 } = node;
  let uses_credentials = false;
  const fetched = [];
  let loaded;
  const page_proxy = new Proxy(page, {
    get: (target, prop, receiver) => {
      if (prop === "query" && prerender_enabled) {
        throw new Error("Cannot access query on a page with prerendering enabled");
      }
      return Reflect.get(target, prop, receiver);
    }
  });
  if (module2.load) {
    const load_input = {
      page: page_proxy,
      get session() {
        uses_credentials = true;
        return $session;
      },
      fetch: async (resource, opts = {}) => {
        let url;
        if (typeof resource === "string") {
          url = resource;
        } else {
          url = resource.url;
          opts = {
            method: resource.method,
            headers: resource.headers,
            body: resource.body,
            mode: resource.mode,
            credentials: resource.credentials,
            cache: resource.cache,
            redirect: resource.redirect,
            referrer: resource.referrer,
            integrity: resource.integrity,
            ...opts
          };
        }
        const resolved = resolve(request.path, url.split("?")[0]);
        let response;
        const filename = resolved.replace(options2.paths.assets, "").slice(1);
        const filename_html = `${filename}/index.html`;
        const asset = options2.manifest.assets.find((d) => d.file === filename || d.file === filename_html);
        if (asset) {
          response = options2.read ? new Response(options2.read(asset.file), {
            headers: asset.type ? { "content-type": asset.type } : {}
          }) : await fetch(`http://${page.host}/${asset.file}`, opts);
        } else if (resolved.startsWith("/") && !resolved.startsWith("//")) {
          const relative = resolved;
          const headers = { ...opts.headers };
          if (opts.credentials !== "omit") {
            uses_credentials = true;
            headers.cookie = request.headers.cookie;
            if (!headers.authorization) {
              headers.authorization = request.headers.authorization;
            }
          }
          if (opts.body && typeof opts.body !== "string") {
            throw new Error("Request body must be a string");
          }
          const search = url.includes("?") ? url.slice(url.indexOf("?") + 1) : "";
          const rendered = await respond({
            host: request.host,
            method: opts.method || "GET",
            headers,
            path: relative,
            rawBody: new TextEncoder().encode(opts.body),
            query: new URLSearchParams(search)
          }, options2, {
            fetched: url,
            initiator: route
          });
          if (rendered) {
            if (state.prerender) {
              state.prerender.dependencies.set(relative, rendered);
            }
            response = new Response(rendered.body, {
              status: rendered.status,
              headers: rendered.headers
            });
          }
        } else {
          if (resolved.startsWith("//")) {
            throw new Error(`Cannot request protocol-relative URL (${url}) in server-side fetch`);
          }
          if (typeof request.host !== "undefined") {
            const { hostname: fetch_hostname } = new URL(url);
            const [server_hostname] = request.host.split(":");
            if (`.${fetch_hostname}`.endsWith(`.${server_hostname}`) && opts.credentials !== "omit") {
              uses_credentials = true;
              opts.headers = {
                ...opts.headers,
                cookie: request.headers.cookie
              };
            }
          }
          const external_request = new Request(url, opts);
          response = await options2.hooks.externalFetch.call(null, external_request);
        }
        if (response) {
          const proxy = new Proxy(response, {
            get(response2, key, receiver) {
              async function text() {
                const body = await response2.text();
                const headers = {};
                for (const [key2, value] of response2.headers) {
                  if (key2 !== "etag" && key2 !== "set-cookie")
                    headers[key2] = value;
                }
                if (!opts.body || typeof opts.body === "string") {
                  fetched.push({
                    url,
                    body: opts.body,
                    json: `{"status":${response2.status},"statusText":${s(response2.statusText)},"headers":${s(headers)},"body":${escape(body)}}`
                  });
                }
                return body;
              }
              if (key === "text") {
                return text;
              }
              if (key === "json") {
                return async () => {
                  return JSON.parse(await text());
                };
              }
              return Reflect.get(response2, key, response2);
            }
          });
          return proxy;
        }
        return response || new Response("Not found", {
          status: 404
        });
      },
      context: { ...context }
    };
    if (is_error) {
      load_input.status = status;
      load_input.error = error3;
    }
    loaded = await module2.load.call(null, load_input);
  } else {
    loaded = {};
  }
  if (!loaded && is_leaf && !is_error)
    return;
  if (!loaded) {
    throw new Error(`${node.entry} - load must return a value except for page fall through`);
  }
  return {
    node,
    loaded: normalize(loaded),
    context: loaded.context || context,
    fetched,
    uses_credentials
  };
}
var escaped = {
  "<": "\\u003C",
  ">": "\\u003E",
  "/": "\\u002F",
  "\\": "\\\\",
  "\b": "\\b",
  "\f": "\\f",
  "\n": "\\n",
  "\r": "\\r",
  "	": "\\t",
  "\0": "\\0",
  "\u2028": "\\u2028",
  "\u2029": "\\u2029"
};
function escape(str) {
  let result = '"';
  for (let i = 0; i < str.length; i += 1) {
    const char = str.charAt(i);
    const code = char.charCodeAt(0);
    if (char === '"') {
      result += '\\"';
    } else if (char in escaped) {
      result += escaped[char];
    } else if (code >= 55296 && code <= 57343) {
      const next = str.charCodeAt(i + 1);
      if (code <= 56319 && next >= 56320 && next <= 57343) {
        result += char + str[++i];
      } else {
        result += `\\u${code.toString(16).toUpperCase()}`;
      }
    } else {
      result += char;
    }
  }
  result += '"';
  return result;
}
var absolute = /^([a-z]+:)?\/?\//;
function resolve(base2, path) {
  const base_match = absolute.exec(base2);
  const path_match = absolute.exec(path);
  if (!base_match) {
    throw new Error(`bad base path: "${base2}"`);
  }
  const baseparts = path_match ? [] : base2.slice(base_match[0].length).split("/");
  const pathparts = path_match ? path.slice(path_match[0].length).split("/") : path.split("/");
  baseparts.pop();
  for (let i = 0; i < pathparts.length; i += 1) {
    const part = pathparts[i];
    if (part === ".")
      continue;
    else if (part === "..")
      baseparts.pop();
    else
      baseparts.push(part);
  }
  const prefix = path_match && path_match[0] || base_match && base_match[0] || "";
  return `${prefix}${baseparts.join("/")}`;
}
function coalesce_to_error(err) {
  return err instanceof Error ? err : new Error(JSON.stringify(err));
}
async function respond_with_error({ request, options: options2, state, $session, status, error: error3 }) {
  const default_layout = await options2.load_component(options2.manifest.layout);
  const default_error = await options2.load_component(options2.manifest.error);
  const page = {
    host: request.host,
    path: request.path,
    query: request.query,
    params: {}
  };
  const loaded = await load_node({
    request,
    options: options2,
    state,
    route: null,
    page,
    node: default_layout,
    $session,
    context: {},
    prerender_enabled: is_prerender_enabled(options2, default_error, state),
    is_leaf: false,
    is_error: false
  });
  const branch = [
    loaded,
    await load_node({
      request,
      options: options2,
      state,
      route: null,
      page,
      node: default_error,
      $session,
      context: loaded ? loaded.context : {},
      prerender_enabled: is_prerender_enabled(options2, default_error, state),
      is_leaf: false,
      is_error: true,
      status,
      error: error3
    })
  ];
  try {
    return await render_response({
      options: options2,
      $session,
      page_config: {
        hydrate: options2.hydrate,
        router: options2.router,
        ssr: options2.ssr
      },
      status,
      error: error3,
      branch,
      page
    });
  } catch (err) {
    const error4 = coalesce_to_error(err);
    options2.handle_error(error4, request);
    return {
      status: 500,
      headers: {},
      body: error4.stack
    };
  }
}
function is_prerender_enabled(options2, node, state) {
  return options2.prerender && (!!node.module.prerender || !!state.prerender && state.prerender.all);
}
async function respond$1(opts) {
  const { request, options: options2, state, $session, route } = opts;
  let nodes;
  try {
    nodes = await Promise.all(route.a.map((id) => id ? options2.load_component(id) : void 0));
  } catch (err) {
    const error4 = coalesce_to_error(err);
    options2.handle_error(error4, request);
    return await respond_with_error({
      request,
      options: options2,
      state,
      $session,
      status: 500,
      error: error4
    });
  }
  const leaf = nodes[nodes.length - 1].module;
  let page_config = get_page_config(leaf, options2);
  if (!leaf.prerender && state.prerender && !state.prerender.all) {
    return {
      status: 204,
      headers: {},
      body: ""
    };
  }
  let branch = [];
  let status = 200;
  let error3;
  ssr:
    if (page_config.ssr) {
      let context = {};
      for (let i = 0; i < nodes.length; i += 1) {
        const node = nodes[i];
        let loaded;
        if (node) {
          try {
            loaded = await load_node({
              ...opts,
              node,
              context,
              prerender_enabled: is_prerender_enabled(options2, node, state),
              is_leaf: i === nodes.length - 1,
              is_error: false
            });
            if (!loaded)
              return;
            if (loaded.loaded.redirect) {
              return {
                status: loaded.loaded.status,
                headers: {
                  location: encodeURI(loaded.loaded.redirect)
                }
              };
            }
            if (loaded.loaded.error) {
              ({ status, error: error3 } = loaded.loaded);
            }
          } catch (err) {
            const e = coalesce_to_error(err);
            options2.handle_error(e, request);
            status = 500;
            error3 = e;
          }
          if (loaded && !error3) {
            branch.push(loaded);
          }
          if (error3) {
            while (i--) {
              if (route.b[i]) {
                const error_node = await options2.load_component(route.b[i]);
                let node_loaded;
                let j = i;
                while (!(node_loaded = branch[j])) {
                  j -= 1;
                }
                try {
                  const error_loaded = await load_node({
                    ...opts,
                    node: error_node,
                    context: node_loaded.context,
                    prerender_enabled: is_prerender_enabled(options2, error_node, state),
                    is_leaf: false,
                    is_error: true,
                    status,
                    error: error3
                  });
                  if (error_loaded.loaded.error) {
                    continue;
                  }
                  page_config = get_page_config(error_node.module, options2);
                  branch = branch.slice(0, j + 1).concat(error_loaded);
                  break ssr;
                } catch (err) {
                  const e = coalesce_to_error(err);
                  options2.handle_error(e, request);
                  continue;
                }
              }
            }
            return await respond_with_error({
              request,
              options: options2,
              state,
              $session,
              status,
              error: error3
            });
          }
        }
        if (loaded && loaded.loaded.context) {
          context = {
            ...context,
            ...loaded.loaded.context
          };
        }
      }
    }
  try {
    return await render_response({
      ...opts,
      page_config,
      status,
      error: error3,
      branch: branch.filter(Boolean)
    });
  } catch (err) {
    const error4 = coalesce_to_error(err);
    options2.handle_error(error4, request);
    return await respond_with_error({
      ...opts,
      status: 500,
      error: error4
    });
  }
}
function get_page_config(leaf, options2) {
  return {
    ssr: "ssr" in leaf ? !!leaf.ssr : options2.ssr,
    router: "router" in leaf ? !!leaf.router : options2.router,
    hydrate: "hydrate" in leaf ? !!leaf.hydrate : options2.hydrate
  };
}
async function render_page(request, route, match, options2, state) {
  if (state.initiator === route) {
    return {
      status: 404,
      headers: {},
      body: `Not found: ${request.path}`
    };
  }
  const params = route.params(match);
  const page = {
    host: request.host,
    path: request.path,
    query: request.query,
    params
  };
  const $session = await options2.hooks.getSession(request);
  const response = await respond$1({
    request,
    options: options2,
    state,
    $session,
    route,
    page
  });
  if (response) {
    return response;
  }
  if (state.fetched) {
    return {
      status: 500,
      headers: {},
      body: `Bad request in load function: failed to fetch ${state.fetched}`
    };
  }
}
function read_only_form_data() {
  const map = new Map();
  return {
    append(key, value) {
      if (map.has(key)) {
        (map.get(key) || []).push(value);
      } else {
        map.set(key, [value]);
      }
    },
    data: new ReadOnlyFormData(map)
  };
}
var ReadOnlyFormData = class {
  #map;
  constructor(map) {
    this.#map = map;
  }
  get(key) {
    const value = this.#map.get(key);
    return value && value[0];
  }
  getAll(key) {
    return this.#map.get(key);
  }
  has(key) {
    return this.#map.has(key);
  }
  *[Symbol.iterator]() {
    for (const [key, value] of this.#map) {
      for (let i = 0; i < value.length; i += 1) {
        yield [key, value[i]];
      }
    }
  }
  *entries() {
    for (const [key, value] of this.#map) {
      for (let i = 0; i < value.length; i += 1) {
        yield [key, value[i]];
      }
    }
  }
  *keys() {
    for (const [key] of this.#map)
      yield key;
  }
  *values() {
    for (const [, value] of this.#map) {
      for (let i = 0; i < value.length; i += 1) {
        yield value[i];
      }
    }
  }
};
function parse_body(raw, headers) {
  if (!raw)
    return raw;
  const content_type = headers["content-type"];
  const [type, ...directives] = content_type ? content_type.split(/;\s*/) : [];
  const text = () => new TextDecoder(headers["content-encoding"] || "utf-8").decode(raw);
  switch (type) {
    case "text/plain":
      return text();
    case "application/json":
      return JSON.parse(text());
    case "application/x-www-form-urlencoded":
      return get_urlencoded(text());
    case "multipart/form-data": {
      const boundary = directives.find((directive) => directive.startsWith("boundary="));
      if (!boundary)
        throw new Error("Missing boundary");
      return get_multipart(text(), boundary.slice("boundary=".length));
    }
    default:
      return raw;
  }
}
function get_urlencoded(text) {
  const { data, append } = read_only_form_data();
  text.replace(/\+/g, " ").split("&").forEach((str) => {
    const [key, value] = str.split("=");
    append(decodeURIComponent(key), decodeURIComponent(value));
  });
  return data;
}
function get_multipart(text, boundary) {
  const parts = text.split(`--${boundary}`);
  if (parts[0] !== "" || parts[parts.length - 1].trim() !== "--") {
    throw new Error("Malformed form data");
  }
  const { data, append } = read_only_form_data();
  parts.slice(1, -1).forEach((part) => {
    const match = /\s*([\s\S]+?)\r\n\r\n([\s\S]*)\s*/.exec(part);
    if (!match) {
      throw new Error("Malformed form data");
    }
    const raw_headers = match[1];
    const body = match[2].trim();
    let key;
    const headers = {};
    raw_headers.split("\r\n").forEach((str) => {
      const [raw_header, ...raw_directives] = str.split("; ");
      let [name, value] = raw_header.split(": ");
      name = name.toLowerCase();
      headers[name] = value;
      const directives = {};
      raw_directives.forEach((raw_directive) => {
        const [name2, value2] = raw_directive.split("=");
        directives[name2] = JSON.parse(value2);
      });
      if (name === "content-disposition") {
        if (value !== "form-data")
          throw new Error("Malformed form data");
        if (directives.filename) {
          throw new Error("File upload is not yet implemented");
        }
        if (directives.name) {
          key = directives.name;
        }
      }
    });
    if (!key)
      throw new Error("Malformed form data");
    append(key, body);
  });
  return data;
}
async function respond(incoming, options2, state = {}) {
  if (incoming.path !== "/" && options2.trailing_slash !== "ignore") {
    const has_trailing_slash = incoming.path.endsWith("/");
    if (has_trailing_slash && options2.trailing_slash === "never" || !has_trailing_slash && options2.trailing_slash === "always" && !(incoming.path.split("/").pop() || "").includes(".")) {
      const path = has_trailing_slash ? incoming.path.slice(0, -1) : incoming.path + "/";
      const q = incoming.query.toString();
      return {
        status: 301,
        headers: {
          location: options2.paths.base + path + (q ? `?${q}` : "")
        }
      };
    }
  }
  const headers = lowercase_keys(incoming.headers);
  const request = {
    ...incoming,
    headers,
    body: parse_body(incoming.rawBody, headers),
    params: {},
    locals: {}
  };
  try {
    return await options2.hooks.handle({
      request,
      resolve: async (request2) => {
        if (state.prerender && state.prerender.fallback) {
          return await render_response({
            options: options2,
            $session: await options2.hooks.getSession(request2),
            page_config: { ssr: false, router: true, hydrate: true },
            status: 200,
            branch: []
          });
        }
        const decoded = decodeURI(request2.path);
        for (const route of options2.manifest.routes) {
          const match = route.pattern.exec(decoded);
          if (!match)
            continue;
          const response = route.type === "endpoint" ? await render_endpoint(request2, route, match) : await render_page(request2, route, match, options2, state);
          if (response) {
            if (response.status === 200) {
              if (!/(no-store|immutable)/.test(response.headers["cache-control"])) {
                const etag = `"${hash(response.body || "")}"`;
                if (request2.headers["if-none-match"] === etag) {
                  return {
                    status: 304,
                    headers: {},
                    body: ""
                  };
                }
                response.headers["etag"] = etag;
              }
            }
            return response;
          }
        }
        const $session = await options2.hooks.getSession(request2);
        return await respond_with_error({
          request: request2,
          options: options2,
          state,
          $session,
          status: 404,
          error: new Error(`Not found: ${request2.path}`)
        });
      }
    });
  } catch (err) {
    const e = coalesce_to_error(err);
    options2.handle_error(e, request);
    return {
      status: 500,
      headers: {},
      body: options2.dev ? e.stack : e.message
    };
  }
}

// .svelte-kit/output/server/app.js
function run(fn) {
  return fn();
}
function blank_object() {
  return Object.create(null);
}
function run_all(fns) {
  fns.forEach(run);
}
function null_to_empty(value) {
  return value == null ? "" : value;
}
var current_component;
function set_current_component(component) {
  current_component = component;
}
function get_current_component() {
  if (!current_component)
    throw new Error("Function called outside component initialization");
  return current_component;
}
function setContext(key, context) {
  get_current_component().$$.context.set(key, context);
}
Promise.resolve();
var escaped2 = {
  '"': "&quot;",
  "'": "&#39;",
  "&": "&amp;",
  "<": "&lt;",
  ">": "&gt;"
};
function escape2(html) {
  return String(html).replace(/["'&<>]/g, (match) => escaped2[match]);
}
function each(items, fn) {
  let str = "";
  for (let i = 0; i < items.length; i += 1) {
    str += fn(items[i], i);
  }
  return str;
}
var missing_component = {
  $$render: () => ""
};
function validate_component(component, name) {
  if (!component || !component.$$render) {
    if (name === "svelte:component")
      name += " this={...}";
    throw new Error(`<${name}> is not a valid SSR component. You may need to review your build config to ensure that dependencies are compiled, rather than imported as pre-compiled modules`);
  }
  return component;
}
var on_destroy;
function create_ssr_component(fn) {
  function $$render(result, props, bindings, slots, context) {
    const parent_component = current_component;
    const $$ = {
      on_destroy,
      context: new Map(parent_component ? parent_component.$$.context : context || []),
      on_mount: [],
      before_update: [],
      after_update: [],
      callbacks: blank_object()
    };
    set_current_component({ $$ });
    const html = fn(result, props, bindings, slots);
    set_current_component(parent_component);
    return html;
  }
  return {
    render: (props = {}, { $$slots = {}, context = new Map() } = {}) => {
      on_destroy = [];
      const result = { title: "", head: "", css: new Set() };
      const html = $$render(result, props, {}, $$slots, context);
      run_all(on_destroy);
      return {
        html,
        css: {
          code: Array.from(result.css).map((css2) => css2.code).join("\n"),
          map: null
        },
        head: result.title + result.head
      };
    },
    $$render
  };
}
function add_attribute(name, value, boolean) {
  if (value == null || boolean && !value)
    return "";
  return ` ${name}${value === true ? "" : `=${typeof value === "string" ? JSON.stringify(escape2(value)) : `"${value}"`}`}`;
}
function afterUpdate() {
}
var css$5 = {
  code: "#svelte-announcer.svelte-1pdgbjn{clip:rect(0 0 0 0);-webkit-clip-path:inset(50%);clip-path:inset(50%);height:1px;left:0;overflow:hidden;position:absolute;top:0;white-space:nowrap;width:1px}",
  map: `{"version":3,"file":"root.svelte","sources":["root.svelte"],"sourcesContent":["<!-- This file is generated by @sveltejs/kit \u2014 do not edit it! -->\\n<script>\\n\\timport { setContext, afterUpdate, onMount } from 'svelte';\\n\\n\\t// stores\\n\\texport let stores;\\n\\texport let page;\\n\\n\\texport let components;\\n\\texport let props_0 = null;\\n\\texport let props_1 = null;\\n\\texport let props_2 = null;\\n\\n\\tsetContext('__svelte__', stores);\\n\\n\\t$: stores.page.set(page);\\n\\tafterUpdate(stores.page.notify);\\n\\n\\tlet mounted = false;\\n\\tlet navigated = false;\\n\\tlet title = null;\\n\\n\\tonMount(() => {\\n\\t\\tconst unsubscribe = stores.page.subscribe(() => {\\n\\t\\t\\tif (mounted) {\\n\\t\\t\\t\\tnavigated = true;\\n\\t\\t\\t\\ttitle = document.title || 'untitled page';\\n\\t\\t\\t}\\n\\t\\t});\\n\\n\\t\\tmounted = true;\\n\\t\\treturn unsubscribe;\\n\\t});\\n<\/script>\\n\\n<svelte:component this={components[0]} {...(props_0 || {})}>\\n\\t{#if components[1]}\\n\\t\\t<svelte:component this={components[1]} {...(props_1 || {})}>\\n\\t\\t\\t{#if components[2]}\\n\\t\\t\\t\\t<svelte:component this={components[2]} {...(props_2 || {})}/>\\n\\t\\t\\t{/if}\\n\\t\\t</svelte:component>\\n\\t{/if}\\n</svelte:component>\\n\\n{#if mounted}\\n\\t<div id=\\"svelte-announcer\\" aria-live=\\"assertive\\" aria-atomic=\\"true\\">\\n\\t\\t{#if navigated}\\n\\t\\t\\t{title}\\n\\t\\t{/if}\\n\\t</div>\\n{/if}\\n\\n<style>#svelte-announcer{clip:rect(0 0 0 0);-webkit-clip-path:inset(50%);clip-path:inset(50%);height:1px;left:0;overflow:hidden;position:absolute;top:0;white-space:nowrap;width:1px}</style>"],"names":[],"mappings":"AAqDO,gCAAiB,CAAC,KAAK,KAAK,CAAC,CAAC,CAAC,CAAC,CAAC,CAAC,CAAC,CAAC,CAAC,kBAAkB,MAAM,GAAG,CAAC,CAAC,UAAU,MAAM,GAAG,CAAC,CAAC,OAAO,GAAG,CAAC,KAAK,CAAC,CAAC,SAAS,MAAM,CAAC,SAAS,QAAQ,CAAC,IAAI,CAAC,CAAC,YAAY,MAAM,CAAC,MAAM,GAAG,CAAC"}`
};
var Root = create_ssr_component(($$result, $$props, $$bindings, slots) => {
  let { stores } = $$props;
  let { page } = $$props;
  let { components } = $$props;
  let { props_0 = null } = $$props;
  let { props_1 = null } = $$props;
  let { props_2 = null } = $$props;
  setContext("__svelte__", stores);
  afterUpdate(stores.page.notify);
  if ($$props.stores === void 0 && $$bindings.stores && stores !== void 0)
    $$bindings.stores(stores);
  if ($$props.page === void 0 && $$bindings.page && page !== void 0)
    $$bindings.page(page);
  if ($$props.components === void 0 && $$bindings.components && components !== void 0)
    $$bindings.components(components);
  if ($$props.props_0 === void 0 && $$bindings.props_0 && props_0 !== void 0)
    $$bindings.props_0(props_0);
  if ($$props.props_1 === void 0 && $$bindings.props_1 && props_1 !== void 0)
    $$bindings.props_1(props_1);
  if ($$props.props_2 === void 0 && $$bindings.props_2 && props_2 !== void 0)
    $$bindings.props_2(props_2);
  $$result.css.add(css$5);
  {
    stores.page.set(page);
  }
  return `


${validate_component(components[0] || missing_component, "svelte:component").$$render($$result, Object.assign(props_0 || {}), {}, {
    default: () => `${components[1] ? `${validate_component(components[1] || missing_component, "svelte:component").$$render($$result, Object.assign(props_1 || {}), {}, {
      default: () => `${components[2] ? `${validate_component(components[2] || missing_component, "svelte:component").$$render($$result, Object.assign(props_2 || {}), {}, {})}` : ``}`
    })}` : ``}`
  })}

${``}`;
});
var base = "";
var assets = "";
function set_paths(paths) {
  base = paths.base;
  assets = paths.assets || base;
}
function set_prerendering(value) {
}
var user_hooks = /* @__PURE__ */ Object.freeze({
  __proto__: null,
  [Symbol.toStringTag]: "Module"
});
var template = ({ head, body }) => '<!DOCTYPE html>\n<html lang="fr">\n    <head>\n        <meta charset="utf-8" />\n        <link rel="icon" href="/favicon.png" />\n        <meta name="viewport" content="width=device-width, initial-scale=1" />\n        <title>SKCommerce</title>\n        <meta name="description"  content="This is going to be awesome"></head>\n        ' + head + '\n    </head>\n    <body>\n        <div id="svelte">' + body + "</div>\n    </body>\n</html>\n";
var options = null;
var default_settings = { paths: { "base": "", "assets": "" } };
function init(settings = default_settings) {
  set_paths(settings.paths);
  set_prerendering(settings.prerendering || false);
  const hooks = get_hooks(user_hooks);
  options = {
    amp: false,
    dev: false,
    entry: {
      file: assets + "/_app/start-e4b14840.js",
      css: [assets + "/_app/assets/start-464e9d0a.css"],
      js: [assets + "/_app/start-e4b14840.js", assets + "/_app/chunks/vendor-bcf034a6.js"]
    },
    fetched: void 0,
    floc: false,
    get_component_path: (id) => assets + "/_app/" + entry_lookup[id],
    get_stack: (error22) => String(error22),
    handle_error: (error22, request) => {
      hooks.handleError({ error: error22, request });
      error22.stack = options.get_stack(error22);
    },
    hooks,
    hydrate: true,
    initiator: void 0,
    load_component,
    manifest,
    paths: settings.paths,
    prerender: true,
    read: settings.read,
    root: Root,
    service_worker: null,
    router: true,
    ssr: true,
    target: "#svelte",
    template,
    trailing_slash: "never"
  };
}
var empty = () => ({});
var manifest = {
  assets: [{ "file": "favicon.png", "size": 1571, "type": "image/png" }, { "file": "logo.svg", "size": 2806, "type": "image/svg+xml" }],
  layout: "src/routes/__layout.svelte",
  error: ".svelte-kit/build/components/error.svelte",
  routes: [
    {
      type: "page",
      pattern: /^\/$/,
      params: empty,
      a: ["src/routes/__layout.svelte", "src/routes/index.svelte"],
      b: [".svelte-kit/build/components/error.svelte"]
    },
    {
      type: "page",
      pattern: /^\/sign-in\/?$/,
      params: empty,
      a: ["src/routes/__layout.svelte", "src/routes/sign-in.svelte"],
      b: [".svelte-kit/build/components/error.svelte"]
    },
    {
      type: "endpoint",
      pattern: /^\/api\/homeCategories\/?$/,
      params: empty,
      load: () => Promise.resolve().then(function() {
        return homeCategories;
      })
    },
    {
      type: "endpoint",
      pattern: /^\/api\/productsBar\/?$/,
      params: empty,
      load: () => Promise.resolve().then(function() {
        return productsBar;
      })
    },
    {
      type: "endpoint",
      pattern: /^\/api\/showcase\/?$/,
      params: empty,
      load: () => Promise.resolve().then(function() {
        return showcase;
      })
    },
    {
      type: "endpoint",
      pattern: /^\/api\/footer\/?$/,
      params: empty,
      load: () => Promise.resolve().then(function() {
        return footer;
      })
    },
    {
      type: "endpoint",
      pattern: /^\/api\/nav\/?$/,
      params: empty,
      load: () => Promise.resolve().then(function() {
        return nav;
      })
    }
  ]
};
var get_hooks = (hooks) => ({
  getSession: hooks.getSession || (() => ({})),
  handle: hooks.handle || (({ request, resolve: resolve2 }) => resolve2(request)),
  handleError: hooks.handleError || (({ error: error22 }) => console.error(error22.stack)),
  externalFetch: hooks.externalFetch || fetch
});
var module_lookup = {
  "src/routes/__layout.svelte": () => Promise.resolve().then(function() {
    return __layout;
  }),
  ".svelte-kit/build/components/error.svelte": () => Promise.resolve().then(function() {
    return error2;
  }),
  "src/routes/index.svelte": () => Promise.resolve().then(function() {
    return index;
  }),
  "src/routes/sign-in.svelte": () => Promise.resolve().then(function() {
    return signIn;
  })
};
var metadata_lookup = { "src/routes/__layout.svelte": { "entry": "pages/__layout.svelte-e268f9da.js", "css": ["assets/pages/__layout.svelte-0c508581.css"], "js": ["pages/__layout.svelte-e268f9da.js", "chunks/vendor-bcf034a6.js"], "styles": [] }, ".svelte-kit/build/components/error.svelte": { "entry": "error.svelte-ae630499.js", "css": [], "js": ["error.svelte-ae630499.js", "chunks/vendor-bcf034a6.js"], "styles": [] }, "src/routes/index.svelte": { "entry": "pages/index.svelte-0fb03e4e.js", "css": [], "js": ["pages/index.svelte-0fb03e4e.js", "chunks/vendor-bcf034a6.js"], "styles": [] }, "src/routes/sign-in.svelte": { "entry": "pages/sign-in.svelte-2fc22973.js", "css": [], "js": ["pages/sign-in.svelte-2fc22973.js", "chunks/vendor-bcf034a6.js"], "styles": [] } };
async function load_component(file) {
  const { entry, css: css2, js, styles } = metadata_lookup[file];
  return {
    module: await module_lookup[file](),
    entry: assets + "/_app/" + entry,
    css: css2.map((dep) => assets + "/_app/" + dep),
    js: js.map((dep) => assets + "/_app/" + dep),
    styles
  };
}
function render(request, {
  prerender
} = {}) {
  const host = request.headers["host"];
  return respond({ ...request, host }, options, { prerender });
}
var homeCategories = /* @__PURE__ */ Object.freeze({
  __proto__: null,
  [Symbol.toStringTag]: "Module"
});
var productsBar = /* @__PURE__ */ Object.freeze({
  __proto__: null,
  [Symbol.toStringTag]: "Module"
});
var showcase = /* @__PURE__ */ Object.freeze({
  __proto__: null,
  [Symbol.toStringTag]: "Module"
});
var footer = /* @__PURE__ */ Object.freeze({
  __proto__: null,
  [Symbol.toStringTag]: "Module"
});
async function get() {
  let headerText = "Livraison en point relais offerte";
  let tabs = [
    {
      label: "Skateboards",
      value: 1,
      items: [
        {
          header: null,
          categories: [
            { label: "Skate complets", link: "/trucks" },
            { label: "Skate enfants", link: "/trucks" },
            { label: "Cruisers", link: "/trucks" }
          ]
        },
        {
          header: "Mat\xE9riel",
          categories: [
            { label: "Decks", link: "/trucks" },
            { label: "Trucks", link: "/trucks" },
            { label: "Roues", link: "/roues" },
            { label: "Roulements", link: "/roues" },
            { label: "Visserie", link: "/roues" },
            { label: "Grips", link: "/grips" },
            { label: "Bushings", link: "/grips" },
            { label: "Pads", link: "/grips" }
          ]
        },
        {
          header: "Accessoires",
          categories: [
            { label: "Tools", link: "/trucks" },
            { label: "Bushings", link: "/trucks" },
            { label: "Goodies", link: "/roues" }
          ]
        }
      ],
      cards: [
        {
          label: "Nouveaut\xE9s skateboard",
          action: "Voir tout",
          url: "google.fr",
          image: "https://images.pexels.com/photos/1870376/pexels-photo-1870376.jpeg",
          alt: "Some alt"
        },
        {
          label: "Nouveaut\xE9s skateboard",
          action: "Voir tout",
          url: "google.fr",
          image: "https://images.pexels.com/photos/1870376/pexels-photo-1870376.jpeg",
          alt: "Some alt"
        }
      ]
    },
    {
      label: "Longboards",
      value: 2,
      items: [
        {
          header: null,
          categories: [
            { label: "Skate complets", link: "/trucks" },
            { label: "Skate enfants", link: "/trucks" },
            { label: "Cruisers", link: "/trucks" }
          ]
        },
        {
          header: "Mat\xE9riel",
          categories: [
            { label: "Decks", link: "/trucks" },
            { label: "Trucks", link: "/trucks" },
            { label: "Roues", link: "/roues" },
            { label: "Roulements", link: "/roues" },
            { label: "Visserie", link: "/roues" },
            { label: "Grips", link: "/grips" },
            { label: "Bushings", link: "/grips" },
            { label: "Pads", link: "/grips" }
          ]
        },
        {
          header: "Accessoires",
          categories: [
            { label: "Tools", link: "/trucks" },
            { label: "Bushings", link: "/trucks" },
            { label: "Goodies", link: "/roues" }
          ]
        }
      ],
      cards: [
        {
          label: "Nouveaut\xE9s skateboard",
          action: "Voir tout",
          link: "google.fr",
          image: "https://images.pexels.com/photos/1870376/pexels-photo-1870376.jpeg",
          alt: "Some alt"
        },
        {
          label: "Nouveaut\xE9s skateboard",
          action: "Voir tout",
          link: "google.fr",
          image: "https://images.pexels.com/photos/1870376/pexels-photo-1870376.jpeg",
          alt: "Some alt"
        }
      ]
    }
  ];
  return {
    body: { tabs, headerText }
  };
}
var nav = /* @__PURE__ */ Object.freeze({
  __proto__: null,
  [Symbol.toStringTag]: "Module",
  get
});
var css$4 = {
  code: ".noHeaderLi.svelte-1wth0i4{--tw-text-opacity:1;color:rgba(17,24,39,var(--tw-text-opacity));font-weight:500}",
  map: '{"version":3,"file":"navItem.svelte","sources":["navItem.svelte"],"sourcesContent":["<script>\\n    let header = \\"mt-6 space-y-6 sm:mt-4 sm:space-y-4\\"\\n    let noHeader = \\"mt-6 space-y-6 sm:mt-0 sm:space-y-4\\"\\n    export let tab\\n<\/script>\\n\\n<div class=\\"overflow-visible absolute top-full inset-x-0 text-sm text-gray-500\\">\\n    <!-- Presentational element used to render the bottom shadow, if we put the shadow on the actual panel it pokes out the top, so we use this shorter element to hide the top of the shadow -->\\n    <div class=\\"absolute inset-0 top-1/2 bg-white shadow\\" aria-hidden=\\"true\\" />\\n\\n    <div class=\\"relative bg-white\\">\\n        <div class=\\"max-w-7xl mx-auto px-8\\">\\n            <div class=\\"grid grid-cols-2 gap-y-10 gap-x-8 py-16\\">\\n                <!-- Showcase-->\\n                <div class=\\"col-start-2 grid grid-cols-2 gap-x-8\\">\\n                    {#each tab.cards as card}\\n                        <div class=\\"group relative text-base sm:text-sm\\">\\n                            <div\\n                                class=\\"aspect-w-1 aspect-h-1 rounded-lg bg-gray-100 overflow-hidden group-hover:opacity-75\\">\\n                                <img\\n                                    src={card.image}\\n                                    alt={card.alt}\\n                                    class=\\"object-center object-cover\\" />\\n                            </div>\\n                            <a\\n                                href={card.link}\\n                                class=\\"mt-6 block font-medium text-gray-900\\">\\n                                <span\\n                                    class=\\"absolute z-10 inset-0\\"\\n                                    aria-hidden=\\"true\\" />\\n                                {card.label}\\n                            </a>\\n                            <p aria-hidden=\\"false\\" class=\\"mt-1\\">\\n                                {card.action}\\n                            </p>\\n                        </div>\\n                    {/each}\\n                </div>\\n                <!-- Menu links-->\\n                <div\\n                    class=\\"row-start-1 grid grid-cols-3 gap-y-10 gap-x-8 text-sm\\">\\n                    {#each tab.items as item}\\n                        <div>\\n                            {#if item.header != null}\\n                                <p\\n                                    id=\\"{item.header}-heading\\"\\n                                    class=\\"font-medium text-gray-800\\">\\n                                    {item.header}\\n                                </p>\\n                            {/if}\\n                            <ul\\n                                role=\\"list\\"\\n                                aria-labelledby=\\"{item.header}-heading\\"\\n                                class={item.header != null ? header : noHeader}>\\n                                {#each item.categories as category}\\n                                    <li class=\\"flex\\">\\n                                        <a\\n                                            href={category.link}\\n                                            class=\\"hover:text-gray-800\\"\\n                                            class:noHeaderLi={item.header ===\\n                                                null}>\\n                                            {category.label}\\n                                        </a>\\n                                    </li>\\n                                {/each}\\n                            </ul>\\n                        </div>\\n                    {/each}\\n                </div>\\n            </div>\\n        </div>\\n    </div>\\n</div>\\n\\n<style>.noHeaderLi{--tw-text-opacity:1;color:rgba(17,24,39,var(--tw-text-opacity));font-weight:500}</style>\\n"],"names":[],"mappings":"AA0EO,0BAAW,CAAC,kBAAkB,CAAC,CAAC,MAAM,KAAK,EAAE,CAAC,EAAE,CAAC,EAAE,CAAC,IAAI,iBAAiB,CAAC,CAAC,CAAC,YAAY,GAAG,CAAC"}'
};
var header$1 = "mt-6 space-y-6 sm:mt-4 sm:space-y-4";
var noHeader$1 = "mt-6 space-y-6 sm:mt-0 sm:space-y-4";
var NavItem = create_ssr_component(($$result, $$props, $$bindings, slots) => {
  let { tab } = $$props;
  if ($$props.tab === void 0 && $$bindings.tab && tab !== void 0)
    $$bindings.tab(tab);
  $$result.css.add(css$4);
  return `<div class="${"overflow-visible absolute top-full inset-x-0 text-sm text-gray-500"}">
    <div class="${"absolute inset-0 top-1/2 bg-white shadow"}" aria-hidden="${"true"}"></div>

    <div class="${"relative bg-white"}"><div class="${"max-w-7xl mx-auto px-8"}"><div class="${"grid grid-cols-2 gap-y-10 gap-x-8 py-16"}">
                <div class="${"col-start-2 grid grid-cols-2 gap-x-8"}">${each(tab.cards, (card) => `<div class="${"group relative text-base sm:text-sm"}"><div class="${"aspect-w-1 aspect-h-1 rounded-lg bg-gray-100 overflow-hidden group-hover:opacity-75"}"><img${add_attribute("src", card.image, 0)}${add_attribute("alt", card.alt, 0)} class="${"object-center object-cover"}"></div>
                            <a${add_attribute("href", card.link, 0)} class="${"mt-6 block font-medium text-gray-900"}"><span class="${"absolute z-10 inset-0"}" aria-hidden="${"true"}"></span>
                                ${escape2(card.label)}</a>
                            <p aria-hidden="${"false"}" class="${"mt-1"}">${escape2(card.action)}</p>
                        </div>`)}</div>
                
                <div class="${"row-start-1 grid grid-cols-3 gap-y-10 gap-x-8 text-sm"}">${each(tab.items, (item) => `<div>${item.header != null ? `<p id="${escape2(item.header) + "-heading"}" class="${"font-medium text-gray-800"}">${escape2(item.header)}
                                </p>` : ``}
                            <ul role="${"list"}" aria-labelledby="${escape2(item.header) + "-heading"}" class="${escape2(null_to_empty(item.header != null ? header$1 : noHeader$1)) + " svelte-1wth0i4"}">${each(item.categories, (category) => `<li class="${"flex"}"><a${add_attribute("href", category.link, 0)} class="${["hover:text-gray-800 svelte-1wth0i4", item.header === null ? "noHeaderLi" : ""].join(" ").trim()}">${escape2(category.label)}</a>
                                    </li>`)}</ul>
                        </div>`)}</div></div></div></div>
</div>`;
});
var css$3 = {
  code: ".selected.svelte-21rnnu{--tw-border-opacity:1;--tw-text-opacity:1;border-color:rgba(79,70,229,var(--tw-border-opacity));color:rgba(79,70,229,var(--tw-text-opacity))}",
  map: '{"version":3,"file":"tabs.svelte","sources":["tabs.svelte"],"sourcesContent":["<script>\\n    import NavItem from \\"/src/components/nav/navItem.svelte\\"\\n    export let data\\n    export let activeTabValue = null\\n    const handleClick = (tabValue) => () => {\\n        if (activeTabValue === tabValue) {\\n            activeTabValue = null\\n        } else {\\n            activeTabValue = tabValue\\n        }\\n    }\\n<\/script>\\n\\n{#each data.tabs as tab}\\n    <div class=\\"flex\\">\\n        <div class=\\"relative flex\\">\\n            <button\\n                on:click={handleClick(tab.value)}\\n                class:selected={tab.value == activeTabValue}\\n                type=\\"button\\"\\n                class=\\"border-transparent text-gray-700 hover:text-gray-900 relative z-10 flex items-center transition-colors ease-out duration-200 text-sm font-medium border-b-2 -mb-px pt-px\\"\\n                aria-expanded={tab.value == activeTabValue}>\\n                {tab.label}\\n            </button>\\n        </div>\\n    </div>\\n{/each}\\n\\n{#each data.tabs as tab}\\n    {#if activeTabValue == tab.value}\\n        <div>\\n            <NavItem {tab} />\\n        </div>\\n    {/if}\\n{/each}\\n\\n<style>.selected{--tw-border-opacity:1;--tw-text-opacity:1;border-color:rgba(79,70,229,var(--tw-border-opacity));color:rgba(79,70,229,var(--tw-text-opacity))}</style>\\n"],"names":[],"mappings":"AAoCO,uBAAS,CAAC,oBAAoB,CAAC,CAAC,kBAAkB,CAAC,CAAC,aAAa,KAAK,EAAE,CAAC,EAAE,CAAC,GAAG,CAAC,IAAI,mBAAmB,CAAC,CAAC,CAAC,MAAM,KAAK,EAAE,CAAC,EAAE,CAAC,GAAG,CAAC,IAAI,iBAAiB,CAAC,CAAC,CAAC"}'
};
var Tabs = create_ssr_component(($$result, $$props, $$bindings, slots) => {
  let { data } = $$props;
  let { activeTabValue = null } = $$props;
  if ($$props.data === void 0 && $$bindings.data && data !== void 0)
    $$bindings.data(data);
  if ($$props.activeTabValue === void 0 && $$bindings.activeTabValue && activeTabValue !== void 0)
    $$bindings.activeTabValue(activeTabValue);
  $$result.css.add(css$3);
  return `${each(data.tabs, (tab) => `<div class="${"flex"}"><div class="${"relative flex"}"><button type="${"button"}" class="${[
    "border-transparent text-gray-700 hover:text-gray-900 relative z-10 flex items-center transition-colors ease-out duration-200 text-sm font-medium border-b-2 -mb-px pt-px svelte-21rnnu",
    tab.value == activeTabValue ? "selected" : ""
  ].join(" ").trim()}"${add_attribute("aria-expanded", tab.value == activeTabValue, 0)}>${escape2(tab.label)}
            </button></div>
    </div>`)}

${each(data.tabs, (tab) => `${activeTabValue == tab.value ? `<div>${validate_component(NavItem, "NavItem").$$render($$result, { tab }, {}, {})}
        </div>` : ``}`)}`;
});
var css$2 = {
  code: ".noHeaderLi.svelte-1wth0i4{--tw-text-opacity:1;color:rgba(17,24,39,var(--tw-text-opacity));font-weight:500}",
  map: '{"version":3,"file":"navItemMobile.svelte","sources":["navItemMobile.svelte"],"sourcesContent":["<script>\\n    let header = \\"mt-6 flex flex-col space-y-6\\"\\n    let noHeader = \\"mt-6 flex flex-col space-y-6\\"\\n    export let tab\\n<\/script>\\n\\n<div\\n    id=\\"tabs-1-panel-1\\"\\n    class=\\"pt-10 pb-8 px-4 space-y-10\\"\\n    aria-labelledby=\\"tabs-1-tab-1\\"\\n    role=\\"tabpanel\\"\\n    tabindex=\\"0\\">\\n    <div class=\\"grid grid-cols-2 gap-x-4\\">\\n        <!--begin-->\\n        {#each tab.cards as card}\\n            <div class=\\"group relative text-sm\\">\\n                <div\\n                    class=\\"aspect-w-1 aspect-h-1 rounded-lg bg-gray-100 overflow-hidden group-hover:opacity-75\\">\\n                    <img\\n                        src={card.image}\\n                        alt={card.alt}\\n                        class=\\"object-center object-cover\\" />\\n                </div>\\n                <a\\n                    href={card.link}\\n                    class=\\"mt-6 block font-medium text-gray-900\\">\\n                    <span class=\\"absolute z-10 inset-0\\" aria-hidden=\\"true\\" />\\n                    {card.label}\\n                </a>\\n                <p aria-hidden=\\"true\\" class=\\"mt-1\\">{card.action}</p>\\n            </div>\\n        {/each}\\n        <!--end-->\\n    </div>\\n    {#each tab.items as item}\\n        <div>\\n            {#if item.header != null}\\n                <p\\n                    id=\\"{item.header}-heading-mobile\\"\\n                    class=\\"font-medium text-gray-800\\">\\n                    {item.header}\\n                </p>\\n            {/if}\\n            <ul\\n                role=\\"list\\"\\n                aria-labelledby=\\"{item.header}-heading-mobile\\"\\n                class={item.header != null ? header : noHeader}>\\n                {#each item.categories as category}\\n                    <li class=\\"flow-root\\">\\n                        <a\\n                            href={category.link}\\n                            class=\\"-m-2 p-2 block text-gray-500\\"\\n                            class:noHeaderLi={item.header === null}>\\n                            {category.label}\\n                        </a>\\n                    </li>\\n                {/each}\\n            </ul>\\n        </div>\\n    {/each}\\n</div>\\n\\n<style>.noHeaderLi{--tw-text-opacity:1;color:rgba(17,24,39,var(--tw-text-opacity));font-weight:500}</style>\\n"],"names":[],"mappings":"AA8DO,0BAAW,CAAC,kBAAkB,CAAC,CAAC,MAAM,KAAK,EAAE,CAAC,EAAE,CAAC,EAAE,CAAC,IAAI,iBAAiB,CAAC,CAAC,CAAC,YAAY,GAAG,CAAC"}'
};
var header = "mt-6 flex flex-col space-y-6";
var noHeader = "mt-6 flex flex-col space-y-6";
var NavItemMobile = create_ssr_component(($$result, $$props, $$bindings, slots) => {
  let { tab } = $$props;
  if ($$props.tab === void 0 && $$bindings.tab && tab !== void 0)
    $$bindings.tab(tab);
  $$result.css.add(css$2);
  return `<div id="${"tabs-1-panel-1"}" class="${"pt-10 pb-8 px-4 space-y-10"}" aria-labelledby="${"tabs-1-tab-1"}" role="${"tabpanel"}" tabindex="${"0"}"><div class="${"grid grid-cols-2 gap-x-4"}">
        ${each(tab.cards, (card) => `<div class="${"group relative text-sm"}"><div class="${"aspect-w-1 aspect-h-1 rounded-lg bg-gray-100 overflow-hidden group-hover:opacity-75"}"><img${add_attribute("src", card.image, 0)}${add_attribute("alt", card.alt, 0)} class="${"object-center object-cover"}"></div>
                <a${add_attribute("href", card.link, 0)} class="${"mt-6 block font-medium text-gray-900"}"><span class="${"absolute z-10 inset-0"}" aria-hidden="${"true"}"></span>
                    ${escape2(card.label)}</a>
                <p aria-hidden="${"true"}" class="${"mt-1"}">${escape2(card.action)}</p>
            </div>`)}
        </div>
    ${each(tab.items, (item) => `<div>${item.header != null ? `<p id="${escape2(item.header) + "-heading-mobile"}" class="${"font-medium text-gray-800"}">${escape2(item.header)}
                </p>` : ``}
            <ul role="${"list"}" aria-labelledby="${escape2(item.header) + "-heading-mobile"}" class="${escape2(null_to_empty(item.header != null ? header : noHeader)) + " svelte-1wth0i4"}">${each(item.categories, (category) => `<li class="${"flow-root"}"><a${add_attribute("href", category.link, 0)} class="${[
    "-m-2 p-2 block text-gray-500 svelte-1wth0i4",
    item.header === null ? "noHeaderLi" : ""
  ].join(" ").trim()}">${escape2(category.label)}</a>
                    </li>`)}</ul>
        </div>`)}
</div>`;
});
var css$1 = {
  code: ".selected.svelte-21rnnu{--tw-border-opacity:1;--tw-text-opacity:1;border-color:rgba(79,70,229,var(--tw-border-opacity));color:rgba(79,70,229,var(--tw-text-opacity))}",
  map: `{"version":3,"file":"tabsMobile.svelte","sources":["tabsMobile.svelte"],"sourcesContent":["<script>\\n    import NavItemMobile from \\"/src/components/nav/navItemMobile.svelte\\"\\n    export let data\\n    export let activeTabValue = null\\n    const handleClick = (tabValue) => () => {\\n        if (activeTabValue === tabValue) {\\n            activeTabValue = null\\n        } else {\\n            activeTabValue = tabValue\\n        }\\n    }\\n<\/script>\\n\\n<div class=\\"mt-2\\">\\n    <div class=\\"border-b border-gray-200\\">\\n        <div class=\\"flex items-center justify-center\\">\\n            <div class=\\" flex flex-1:items-center justify-end space-x-6 px-4\\">\\n                <a\\n                    href=\\"sign-in\\"\\n                    class=\\"px-1 text-sm font-medium text-gray-700 hover:text-gray-800\\"\\n                    >Se connecter</a>\\n                <span class=\\"h-6 w-px bg-gray-200\\" aria-hidden=\\"true\\" />\\n                <a\\n                    href=\\"#\\"\\n                    class=\\"px-1 text-sm font-medium text-gray-700 hover:text-gray-800\\"\\n                    >S'inscrire</a>\\n            </div>\\n        </div>\\n        <div\\n            class=\\"-mb-px flex flex-col px-4\\"\\n            aria-orientation=\\"vertical\\"\\n            role=\\"tablist\\">\\n            {#each data.tabs as tab}\\n                <button\\n                    on:click={handleClick(tab.value)}\\n                    class:selected={tab.value == activeTabValue}\\n                    id=\\"button-{tab.value}\\"\\n                    class=\\"text-gray-900 text-left border-transparent flex-1 whitespace-nowrap py-4 px-1 border-b-2 text-base font-medium\\"\\n                    aria-controls=\\"button-{tab.value}\\"\\n                    aria-expanded={tab.value == activeTabValue}\\n                    role=\\"tab\\"\\n                    type=\\"button\\">\\n                    {tab.label}\\n                </button>\\n                {#if activeTabValue == tab.value}\\n                    <div>\\n                        <NavItemMobile {tab} />\\n                    </div>\\n                {/if}\\n            {/each}\\n        </div>\\n    </div>\\n</div>\\n\\n<style>.selected{--tw-border-opacity:1;--tw-text-opacity:1;border-color:rgba(79,70,229,var(--tw-border-opacity));color:rgba(79,70,229,var(--tw-text-opacity))}</style>\\n"],"names":[],"mappings":"AAsDO,uBAAS,CAAC,oBAAoB,CAAC,CAAC,kBAAkB,CAAC,CAAC,aAAa,KAAK,EAAE,CAAC,EAAE,CAAC,GAAG,CAAC,IAAI,mBAAmB,CAAC,CAAC,CAAC,MAAM,KAAK,EAAE,CAAC,EAAE,CAAC,GAAG,CAAC,IAAI,iBAAiB,CAAC,CAAC,CAAC"}`
};
var TabsMobile = create_ssr_component(($$result, $$props, $$bindings, slots) => {
  let { data } = $$props;
  let { activeTabValue = null } = $$props;
  if ($$props.data === void 0 && $$bindings.data && data !== void 0)
    $$bindings.data(data);
  if ($$props.activeTabValue === void 0 && $$bindings.activeTabValue && activeTabValue !== void 0)
    $$bindings.activeTabValue(activeTabValue);
  $$result.css.add(css$1);
  return `<div class="${"mt-2"}"><div class="${"border-b border-gray-200"}"><div class="${"flex items-center justify-center"}"><div class="${"flex flex-1:items-center justify-end space-x-6 px-4"}"><a href="${"sign-in"}" class="${"px-1 text-sm font-medium text-gray-700 hover:text-gray-800"}">Se connecter</a>
                <span class="${"h-6 w-px bg-gray-200"}" aria-hidden="${"true"}"></span>
                <a href="${"#"}" class="${"px-1 text-sm font-medium text-gray-700 hover:text-gray-800"}">S&#39;inscrire</a></div></div>
        <div class="${"-mb-px flex flex-col px-4"}" aria-orientation="${"vertical"}" role="${"tablist"}">${each(data.tabs, (tab) => `<button id="${"button-" + escape2(tab.value)}" class="${[
    "text-gray-900 text-left border-transparent flex-1 whitespace-nowrap py-4 px-1 border-b-2 text-base font-medium svelte-21rnnu",
    tab.value == activeTabValue ? "selected" : ""
  ].join(" ").trim()}" aria-controls="${"button-" + escape2(tab.value)}"${add_attribute("aria-expanded", tab.value == activeTabValue, 0)} role="${"tab"}" type="${"button"}">${escape2(tab.label)}</button>
                ${activeTabValue == tab.value ? `<div>${validate_component(NavItemMobile, "NavItemMobile").$$render($$result, { tab }, {}, {})}
                    </div>` : ``}`)}</div></div>
</div>`;
});
var css = {
  code: ".mobileMenu.svelte-1avcj8r{opacity:1;visibility:visible}",
  map: `{"version":3,"file":"nav.svelte","sources":["nav.svelte"],"sourcesContent":["<script>\\n    import Tabs from \\"/src/components/nav/tabs.svelte\\"\\n    import TabsMobile from \\"/src/components/nav/tabsMobile.svelte\\"\\n    export let data\\n    let mobileMenu = false\\n    function toggleMobileMenu() {\\n        mobileMenu = !mobileMenu\\n    }\\n<\/script>\\n\\n<div class=\\"bg-white\\">\\n    <div\\n        class=\\"fixed flex inset-0 z-40 transition-opacity ease-linear duration-300 opacity-0 invisible lg:hidden\\"\\n        class:mobileMenu\\n        role=\\"dialog\\"\\n        aria-modal=\\"true\\">\\n        <div\\n            class=\\"relative max-w-xs w-full bg-white shadow-xl pb-12 flex flex-col overflow-y-auto\\">\\n            <div class=\\"px-4 pt-5 pb-2 flex\\">\\n                <button\\n                    on:click={toggleMobileMenu}\\n                    type=\\"button\\"\\n                    class=\\"-m-2 p-2 rounded-md inline-flex items-center justify-center text-gray-400\\">\\n                    <span class=\\"sr-only\\">Fermer le menu</span>\\n                    <!-- Heroicon name: outline/x -->\\n                    <svg\\n                        class=\\"h-6 w-6\\"\\n                        xmlns=\\"http://www.w3.org/2000/svg\\"\\n                        fill=\\"none\\"\\n                        viewBox=\\"0 0 24 24\\"\\n                        stroke=\\"currentColor\\"\\n                        aria-hidden=\\"true\\">\\n                        <path\\n                            stroke-linecap=\\"round\\"\\n                            stroke-linejoin=\\"round\\"\\n                            stroke-width=\\"2\\"\\n                            d=\\"M6 18L18 6M6 6l12 12\\" />\\n                    </svg>\\n                </button>\\n            </div>\\n            <TabsMobile {data} />\\n        </div>\\n    </div>\\n    <header class=\\"relative bg-white\\">\\n        <p\\n            class=\\"bg-topBar h-10 flex items-center justify-center text-sm font-medium text-white px-4 sm:px-6 lg:px-8\\">\\n            {data.headerText}\\n        </p>\\n\\n        <nav aria-label=\\"Top\\" class=\\"max-w-7xl mx-auto px-4 sm:px-6 lg:px-8\\">\\n            <div class=\\"border-b border-gray-200\\">\\n                <div class=\\"h-16 flex items-center\\">\\n                    <!-- Mobile menu toggle, controls the 'mobileMenuOpen' state. -->\\n                    <button\\n                        on:click={toggleMobileMenu}\\n                        type=\\"button\\"\\n                        class=\\"bg-white p-2 rounded-md text-gray-400 lg:hidden\\">\\n                        <span class=\\"sr-only\\">Menu</span>\\n                        <!-- Heroicon name: outline/menu -->\\n                        <svg\\n                            class=\\"h-6 w-6\\"\\n                            xmlns=\\"http://www.w3.org/2000/svg\\"\\n                            fill=\\"none\\"\\n                            viewBox=\\"0 0 24 24\\"\\n                            stroke=\\"currentColor\\"\\n                            aria-hidden=\\"true\\">\\n                            <path\\n                                stroke-linecap=\\"round\\"\\n                                stroke-linejoin=\\"round\\"\\n                                stroke-width=\\"2\\"\\n                                d=\\"M4 6h16M4 12h16M4 18h16\\" />\\n                        </svg>\\n                    </button>\\n\\n                    <!-- Logo -->\\n                    <div class=\\"ml-4 flex lg:ml-0\\">\\n                        <a href=\\"#\\">\\n                            <span class=\\"sr-only\\">Workflow</span>\\n                            <img\\n                                class=\\"h-8 w-auto\\"\\n                                src=\\"logo.svg\\"\\n                                alt=\\"numero4 skateshop logo\\" />\\n                        </a>\\n                    </div>\\n                    <div class=\\"hidden lg:ml-8 lg:block lg:self-stretch\\">\\n                        <div class=\\"h-full flex space-x-8\\">\\n                            <Tabs {data} />\\n                        </div>\\n                    </div>\\n                    <div class=\\"ml-auto flex items-center\\">\\n                        <div\\n                            class=\\"hidden lg:flex lg:flex-1 lg:items-center lg:justify-end lg:space-x-6\\">\\n                            <a\\n                                href=\\"sign-in\\"\\n                                class=\\"text-sm font-medium text-gray-700 hover:text-gray-800\\"\\n                                >Se connecter</a>\\n                            <span\\n                                class=\\"h-6 w-px bg-gray-200\\"\\n                                aria-hidden=\\"true\\" />\\n                            <a\\n                                href=\\"#\\"\\n                                class=\\"text-sm font-medium text-gray-700 hover:text-gray-800\\"\\n                                >S'inscrire</a>\\n                        </div>\\n\\n                        <!-- Search -->\\n                        <div class=\\"flex lg:ml-6\\">\\n                            <a\\n                                href=\\"#\\"\\n                                class=\\"p-2 text-gray-400 hover:text-gray-500\\">\\n                                <span class=\\"sr-only\\">Rechercher</span>\\n                                <!-- Heroicon name: outline/search -->\\n                                <svg\\n                                    class=\\"w-6 h-6\\"\\n                                    xmlns=\\"http://www.w3.org/2000/svg\\"\\n                                    fill=\\"none\\"\\n                                    viewBox=\\"0 0 24 24\\"\\n                                    stroke=\\"currentColor\\"\\n                                    aria-hidden=\\"true\\">\\n                                    <path\\n                                        stroke-linecap=\\"round\\"\\n                                        stroke-linejoin=\\"round\\"\\n                                        stroke-width=\\"2\\"\\n                                        d=\\"M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z\\" />\\n                                </svg>\\n                            </a>\\n                        </div>\\n\\n                        <!-- Cart -->\\n                        <div class=\\"ml-4 flow-root lg:ml-6\\">\\n                            <a\\n                                href=\\"#\\"\\n                                class=\\"group -m-2 p-2 flex items-center\\">\\n                                <!-- Heroicon name: outline/shopping-bag -->\\n                                <svg\\n                                    class=\\"flex-shink-0 h-6 w-6 text-gray-400 group-hover:text-gray-500\\"\\n                                    xmlns=\\"http://www.w3.org/2000/svg\\"\\n                                    fill=\\"none\\"\\n                                    viewBox=\\"0 0 24 24\\"\\n                                    stroke=\\"currentColor\\"\\n                                    aria-hidden=\\"true\\">\\n                                    <path\\n                                        stroke-linecap=\\"round\\"\\n                                        stroke-linejoin=\\"round\\"\\n                                        stroke-width=\\"2\\"\\n                                        d=\\"M16 11V7a4 4 0 00-8 0v4M5 9h14l1 12H4L5 9z\\" />\\n                                </svg>\\n                                <span\\n                                    class=\\"ml-2 text-sm font-medium text-gray-700 group-hover:text-gray-800\\"\\n                                    >0</span>\\n                                <span class=\\"sr-only\\"\\n                                    >produits dans le panier</span>\\n                            </a>\\n                        </div>\\n                    </div>\\n                </div>\\n            </div>\\n        </nav>\\n    </header>\\n</div>\\n\\n<style>.mobileMenu{opacity:1;visibility:visible}</style>\\n"],"names":[],"mappings":"AAiKO,0BAAW,CAAC,QAAQ,CAAC,CAAC,WAAW,OAAO,CAAC"}`
};
var Nav = create_ssr_component(($$result, $$props, $$bindings, slots) => {
  let { data } = $$props;
  if ($$props.data === void 0 && $$bindings.data && data !== void 0)
    $$bindings.data(data);
  $$result.css.add(css);
  return `<div class="${"bg-white"}"><div class="${[
    "fixed flex inset-0 z-40 transition-opacity ease-linear duration-300 opacity-0 invisible lg:hidden svelte-1avcj8r",
    ""
  ].join(" ").trim()}" role="${"dialog"}" aria-modal="${"true"}"><div class="${"relative max-w-xs w-full bg-white shadow-xl pb-12 flex flex-col overflow-y-auto"}"><div class="${"px-4 pt-5 pb-2 flex"}"><button type="${"button"}" class="${"-m-2 p-2 rounded-md inline-flex items-center justify-center text-gray-400"}"><span class="${"sr-only"}">Fermer le menu</span>
                    
                    <svg class="${"h-6 w-6"}" xmlns="${"http://www.w3.org/2000/svg"}" fill="${"none"}" viewBox="${"0 0 24 24"}" stroke="${"currentColor"}" aria-hidden="${"true"}"><path stroke-linecap="${"round"}" stroke-linejoin="${"round"}" stroke-width="${"2"}" d="${"M6 18L18 6M6 6l12 12"}"></path></svg></button></div>
            ${validate_component(TabsMobile, "TabsMobile").$$render($$result, { data }, {}, {})}</div></div>
    <header class="${"relative bg-white"}"><p class="${"bg-topBar h-10 flex items-center justify-center text-sm font-medium text-white px-4 sm:px-6 lg:px-8"}">${escape2(data.headerText)}</p>

        <nav aria-label="${"Top"}" class="${"max-w-7xl mx-auto px-4 sm:px-6 lg:px-8"}"><div class="${"border-b border-gray-200"}"><div class="${"h-16 flex items-center"}">
                    <button type="${"button"}" class="${"bg-white p-2 rounded-md text-gray-400 lg:hidden"}"><span class="${"sr-only"}">Menu</span>
                        
                        <svg class="${"h-6 w-6"}" xmlns="${"http://www.w3.org/2000/svg"}" fill="${"none"}" viewBox="${"0 0 24 24"}" stroke="${"currentColor"}" aria-hidden="${"true"}"><path stroke-linecap="${"round"}" stroke-linejoin="${"round"}" stroke-width="${"2"}" d="${"M4 6h16M4 12h16M4 18h16"}"></path></svg></button>

                    
                    <div class="${"ml-4 flex lg:ml-0"}"><a href="${"#"}"><span class="${"sr-only"}">Workflow</span>
                            <img class="${"h-8 w-auto"}" src="${"logo.svg"}" alt="${"numero4 skateshop logo"}"></a></div>
                    <div class="${"hidden lg:ml-8 lg:block lg:self-stretch"}"><div class="${"h-full flex space-x-8"}">${validate_component(Tabs, "Tabs").$$render($$result, { data }, {}, {})}</div></div>
                    <div class="${"ml-auto flex items-center"}"><div class="${"hidden lg:flex lg:flex-1 lg:items-center lg:justify-end lg:space-x-6"}"><a href="${"sign-in"}" class="${"text-sm font-medium text-gray-700 hover:text-gray-800"}">Se connecter</a>
                            <span class="${"h-6 w-px bg-gray-200"}" aria-hidden="${"true"}"></span>
                            <a href="${"#"}" class="${"text-sm font-medium text-gray-700 hover:text-gray-800"}">S&#39;inscrire</a></div>

                        
                        <div class="${"flex lg:ml-6"}"><a href="${"#"}" class="${"p-2 text-gray-400 hover:text-gray-500"}"><span class="${"sr-only"}">Rechercher</span>
                                
                                <svg class="${"w-6 h-6"}" xmlns="${"http://www.w3.org/2000/svg"}" fill="${"none"}" viewBox="${"0 0 24 24"}" stroke="${"currentColor"}" aria-hidden="${"true"}"><path stroke-linecap="${"round"}" stroke-linejoin="${"round"}" stroke-width="${"2"}" d="${"M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"}"></path></svg></a></div>

                        
                        <div class="${"ml-4 flow-root lg:ml-6"}"><a href="${"#"}" class="${"group -m-2 p-2 flex items-center"}">
                                <svg class="${"flex-shink-0 h-6 w-6 text-gray-400 group-hover:text-gray-500"}" xmlns="${"http://www.w3.org/2000/svg"}" fill="${"none"}" viewBox="${"0 0 24 24"}" stroke="${"currentColor"}" aria-hidden="${"true"}"><path stroke-linecap="${"round"}" stroke-linejoin="${"round"}" stroke-width="${"2"}" d="${"M16 11V7a4 4 0 00-8 0v4M5 9h14l1 12H4L5 9z"}"></path></svg>
                                <span class="${"ml-2 text-sm font-medium text-gray-700 group-hover:text-gray-800"}">0</span>
                                <span class="${"sr-only"}">produits dans le panier</span></a></div></div></div></div></nav></header>
</div>`;
});
var Footer = create_ssr_component(($$result, $$props, $$bindings, slots) => {
  return `
<footer class="${"bg-white"}" aria-labelledby="${"footer-heading"}"><h2 id="${"footer-heading"}" class="${"sr-only"}">Footer</h2>
    <div class="${"max-w-7xl mx-auto py-12 px-4 sm:px-6 lg:py-16 lg:px-8"}"><div class="${"xl:grid xl:grid-cols-3 xl:gap-8"}"><div class="${"space-y-8 xl:col-span-1"}"><img class="${"h-10"}" src="${"https://tailwindui.com/img/logos/workflow-mark-gray-300.svg"}" alt="${"Company name"}">
                <p class="${"text-gray-500 text-base"}">Making the world a better place through constructing elegant
                    hierarchies.
                </p>
                <div class="${"flex space-x-6"}"><a href="${"#"}" class="${"text-gray-400 hover:text-gray-500"}"><span class="${"sr-only"}">Facebook</span>
                        <svg class="${"h-6 w-6"}" fill="${"currentColor"}" viewBox="${"0 0 24 24"}" aria-hidden="${"true"}"><path fill-rule="${"evenodd"}" d="${"M22 12c0-5.523-4.477-10-10-10S2 6.477 2 12c0 4.991 3.657 9.128 8.438 9.878v-6.987h-2.54V12h2.54V9.797c0-2.506 1.492-3.89 3.777-3.89 1.094 0 2.238.195 2.238.195v2.46h-1.26c-1.243 0-1.63.771-1.63 1.562V12h2.773l-.443 2.89h-2.33v6.988C18.343 21.128 22 16.991 22 12z"}" clip-rule="${"evenodd"}"></path></svg></a>

                    <a href="${"#"}" class="${"text-gray-400 hover:text-gray-500"}"><span class="${"sr-only"}">Instagram</span>
                        <svg class="${"h-6 w-6"}" fill="${"currentColor"}" viewBox="${"0 0 24 24"}" aria-hidden="${"true"}"><path fill-rule="${"evenodd"}" d="${"M12.315 2c2.43 0 2.784.013 3.808.06 1.064.049 1.791.218 2.427.465a4.902 4.902 0 011.772 1.153 4.902 4.902 0 011.153 1.772c.247.636.416 1.363.465 2.427.048 1.067.06 1.407.06 4.123v.08c0 2.643-.012 2.987-.06 4.043-.049 1.064-.218 1.791-.465 2.427a4.902 4.902 0 01-1.153 1.772 4.902 4.902 0 01-1.772 1.153c-.636.247-1.363.416-2.427.465-1.067.048-1.407.06-4.123.06h-.08c-2.643 0-2.987-.012-4.043-.06-1.064-.049-1.791-.218-2.427-.465a4.902 4.902 0 01-1.772-1.153 4.902 4.902 0 01-1.153-1.772c-.247-.636-.416-1.363-.465-2.427-.047-1.024-.06-1.379-.06-3.808v-.63c0-2.43.013-2.784.06-3.808.049-1.064.218-1.791.465-2.427a4.902 4.902 0 011.153-1.772A4.902 4.902 0 015.45 2.525c.636-.247 1.363-.416 2.427-.465C8.901 2.013 9.256 2 11.685 2h.63zm-.081 1.802h-.468c-2.456 0-2.784.011-3.807.058-.975.045-1.504.207-1.857.344-.467.182-.8.398-1.15.748-.35.35-.566.683-.748 1.15-.137.353-.3.882-.344 1.857-.047 1.023-.058 1.351-.058 3.807v.468c0 2.456.011 2.784.058 3.807.045.975.207 1.504.344 1.857.182.466.399.8.748 1.15.35.35.683.566 1.15.748.353.137.882.3 1.857.344 1.054.048 1.37.058 4.041.058h.08c2.597 0 2.917-.01 3.96-.058.976-.045 1.505-.207 1.858-.344.466-.182.8-.398 1.15-.748.35-.35.566-.683.748-1.15.137-.353.3-.882.344-1.857.048-1.055.058-1.37.058-4.041v-.08c0-2.597-.01-2.917-.058-3.96-.045-.976-.207-1.505-.344-1.858a3.097 3.097 0 00-.748-1.15 3.098 3.098 0 00-1.15-.748c-.353-.137-.882-.3-1.857-.344-1.023-.047-1.351-.058-3.807-.058zM12 6.865a5.135 5.135 0 110 10.27 5.135 5.135 0 010-10.27zm0 1.802a3.333 3.333 0 100 6.666 3.333 3.333 0 000-6.666zm5.338-3.205a1.2 1.2 0 110 2.4 1.2 1.2 0 010-2.4z"}" clip-rule="${"evenodd"}"></path></svg></a>

                    <a href="${"#"}" class="${"text-gray-400 hover:text-gray-500"}"><span class="${"sr-only"}">Twitter</span>
                        <svg class="${"h-6 w-6"}" fill="${"currentColor"}" viewBox="${"0 0 24 24"}" aria-hidden="${"true"}"><path d="${"M8.29 20.251c7.547 0 11.675-6.253 11.675-11.675 0-.178 0-.355-.012-.53A8.348 8.348 0 0022 5.92a8.19 8.19 0 01-2.357.646 4.118 4.118 0 001.804-2.27 8.224 8.224 0 01-2.605.996 4.107 4.107 0 00-6.993 3.743 11.65 11.65 0 01-8.457-4.287 4.106 4.106 0 001.27 5.477A4.072 4.072 0 012.8 9.713v.052a4.105 4.105 0 003.292 4.022 4.095 4.095 0 01-1.853.07 4.108 4.108 0 003.834 2.85A8.233 8.233 0 012 18.407a11.616 11.616 0 006.29 1.84"}"></path></svg></a>

                    <a href="${"#"}" class="${"text-gray-400 hover:text-gray-500"}"><span class="${"sr-only"}">GitHub</span>
                        <svg class="${"h-6 w-6"}" fill="${"currentColor"}" viewBox="${"0 0 24 24"}" aria-hidden="${"true"}"><path fill-rule="${"evenodd"}" d="${"M12 2C6.477 2 2 6.484 2 12.017c0 4.425 2.865 8.18 6.839 9.504.5.092.682-.217.682-.483 0-.237-.008-.868-.013-1.703-2.782.605-3.369-1.343-3.369-1.343-.454-1.158-1.11-1.466-1.11-1.466-.908-.62.069-.608.069-.608 1.003.07 1.531 1.032 1.531 1.032.892 1.53 2.341 1.088 2.91.832.092-.647.35-1.088.636-1.338-2.22-.253-4.555-1.113-4.555-4.951 0-1.093.39-1.988 1.029-2.688-.103-.253-.446-1.272.098-2.65 0 0 .84-.27 2.75 1.026A9.564 9.564 0 0112 6.844c.85.004 1.705.115 2.504.337 1.909-1.296 2.747-1.027 2.747-1.027.546 1.379.202 2.398.1 2.651.64.7 1.028 1.595 1.028 2.688 0 3.848-2.339 4.695-4.566 4.943.359.309.678.92.678 1.855 0 1.338-.012 2.419-.012 2.747 0 .268.18.58.688.482A10.019 10.019 0 0022 12.017C22 6.484 17.522 2 12 2z"}" clip-rule="${"evenodd"}"></path></svg></a>

                    <a href="${"#"}" class="${"text-gray-400 hover:text-gray-500"}"><span class="${"sr-only"}">Dribbble</span>
                        <svg class="${"h-6 w-6"}" fill="${"currentColor"}" viewBox="${"0 0 24 24"}" aria-hidden="${"true"}"><path fill-rule="${"evenodd"}" d="${"M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10c5.51 0 10-4.48 10-10S17.51 2 12 2zm6.605 4.61a8.502 8.502 0 011.93 5.314c-.281-.054-3.101-.629-5.943-.271-.065-.141-.12-.293-.184-.445a25.416 25.416 0 00-.564-1.236c3.145-1.28 4.577-3.124 4.761-3.362zM12 3.475c2.17 0 4.154.813 5.662 2.148-.152.216-1.443 1.941-4.48 3.08-1.399-2.57-2.95-4.675-3.189-5A8.687 8.687 0 0112 3.475zm-3.633.803a53.896 53.896 0 013.167 4.935c-3.992 1.063-7.517 1.04-7.896 1.04a8.581 8.581 0 014.729-5.975zM3.453 12.01v-.26c.37.01 4.512.065 8.775-1.215.25.477.477.965.694 1.453-.109.033-.228.065-.336.098-4.404 1.42-6.747 5.303-6.942 5.629a8.522 8.522 0 01-2.19-5.705zM12 20.547a8.482 8.482 0 01-5.239-1.8c.152-.315 1.888-3.656 6.703-5.337.022-.01.033-.01.054-.022a35.318 35.318 0 011.823 6.475 8.4 8.4 0 01-3.341.684zm4.761-1.465c-.086-.52-.542-3.015-1.659-6.084 2.679-.423 5.022.271 5.314.369a8.468 8.468 0 01-3.655 5.715z"}" clip-rule="${"evenodd"}"></path></svg></a></div></div>
            <div class="${"mt-12 grid grid-cols-2 gap-8 xl:mt-0 xl:col-span-2"}"><div class="${"md:grid md:grid-cols-2 md:gap-8"}"><div><h3 class="${"text-sm font-semibold text-gray-400 tracking-wider uppercase"}">Solutions
                        </h3>
                        <ul role="${"list"}" class="${"mt-4 space-y-4"}"><li><a href="${"#"}" class="${"text-base text-gray-500 hover:text-gray-900"}">Marketing
                                </a></li>

                            <li><a href="${"#"}" class="${"text-base text-gray-500 hover:text-gray-900"}">Analytics
                                </a></li>

                            <li><a href="${"#"}" class="${"text-base text-gray-500 hover:text-gray-900"}">Commerce
                                </a></li>

                            <li><a href="${"#"}" class="${"text-base text-gray-500 hover:text-gray-900"}">Insights
                                </a></li></ul></div>
                    <div class="${"mt-12 md:mt-0"}"><h3 class="${"text-sm font-semibold text-gray-400 tracking-wider uppercase"}">Support
                        </h3>
                        <ul role="${"list"}" class="${"mt-4 space-y-4"}"><li><a href="${"#"}" class="${"text-base text-gray-500 hover:text-gray-900"}">Pricing
                                </a></li>

                            <li><a href="${"#"}" class="${"text-base text-gray-500 hover:text-gray-900"}">Documentation
                                </a></li>

                            <li><a href="${"#"}" class="${"text-base text-gray-500 hover:text-gray-900"}">Guides
                                </a></li>

                            <li><a href="${"#"}" class="${"text-base text-gray-500 hover:text-gray-900"}">API Status
                                </a></li></ul></div></div>
                <div class="${"md:grid md:grid-cols-2 md:gap-8"}"><div><h3 class="${"text-sm font-semibold text-gray-400 tracking-wider uppercase"}">Company
                        </h3>
                        <ul role="${"list"}" class="${"mt-4 space-y-4"}"><li><a href="${"#"}" class="${"text-base text-gray-500 hover:text-gray-900"}">About
                                </a></li>

                            <li><a href="${"#"}" class="${"text-base text-gray-500 hover:text-gray-900"}">Blog
                                </a></li>

                            <li><a href="${"#"}" class="${"text-base text-gray-500 hover:text-gray-900"}">Jobs
                                </a></li>

                            <li><a href="${"#"}" class="${"text-base text-gray-500 hover:text-gray-900"}">Press
                                </a></li>

                            <li><a href="${"#"}" class="${"text-base text-gray-500 hover:text-gray-900"}">Partners
                                </a></li></ul></div>
                    <div class="${"mt-12 md:mt-0"}"><h3 class="${"text-sm font-semibold text-gray-400 tracking-wider uppercase"}">Legal
                        </h3>
                        <ul role="${"list"}" class="${"mt-4 space-y-4"}"><li><a href="${"#"}" class="${"text-base text-gray-500 hover:text-gray-900"}">Claim
                                </a></li>

                            <li><a href="${"#"}" class="${"text-base text-gray-500 hover:text-gray-900"}">Privacy
                                </a></li>

                            <li><a href="${"#"}" class="${"text-base text-gray-500 hover:text-gray-900"}">Terms
                                </a></li></ul></div></div></div></div>
        <div class="${"mt-12 border-t border-gray-200 pt-8"}"><p class="${"text-base text-gray-400 xl:text-center"}">\xA9 2020 Workflow, Inc. All rights reserved.
            </p></div></div></footer>`;
});
async function load$1({ fetch: fetch2 }) {
  const url = "/api/nav";
  const res = await fetch2(url);
  const data = await res.json();
  return { props: { data } };
}
var _layout = create_ssr_component(($$result, $$props, $$bindings, slots) => {
  let { data } = $$props;
  if ($$props.data === void 0 && $$bindings.data && data !== void 0)
    $$bindings.data(data);
  return `${validate_component(Nav, "Nav").$$render($$result, { data }, {}, {})}
${slots.default ? slots.default({}) : ``}
${validate_component(Footer, "Footer").$$render($$result, {}, {}, {})}`;
});
var __layout = /* @__PURE__ */ Object.freeze({
  __proto__: null,
  [Symbol.toStringTag]: "Module",
  "default": _layout,
  load: load$1
});
function load({ error: error22, status }) {
  return { props: { error: error22, status } };
}
var Error$1 = create_ssr_component(($$result, $$props, $$bindings, slots) => {
  let { status } = $$props;
  let { error: error22 } = $$props;
  if ($$props.status === void 0 && $$bindings.status && status !== void 0)
    $$bindings.status(status);
  if ($$props.error === void 0 && $$bindings.error && error22 !== void 0)
    $$bindings.error(error22);
  return `<h1>${escape2(status)}</h1>

<pre>${escape2(error22.message)}</pre>



${error22.frame ? `<pre>${escape2(error22.frame)}</pre>` : ``}
${error22.stack ? `<pre>${escape2(error22.stack)}</pre>` : ``}`;
});
var error2 = /* @__PURE__ */ Object.freeze({
  __proto__: null,
  [Symbol.toStringTag]: "Module",
  "default": Error$1,
  load
});
var Showcase = create_ssr_component(($$result, $$props, $$bindings, slots) => {
  return `
<div class="${"bg-gray-50"}"><div class="${"max-w-7xl mx-auto py-8 px-4 sm:py-8 sm:px-6 lg:px-8"}"><div class="${"sm:flex sm:items-baseline sm:justify-between"}"><h2 class="${"text-2xl font-extrabold tracking-tight text-gray-900"}">Nouveaut\xE9s &amp; tendances
            </h2>
            <a href="${"#"}" class="${"hidden text-sm font-semibold text-indigo-600 hover:text-indigo-500 sm:block"}">Voir toutes les nouveaut\xE9s &amp; tendances<span aria-hidden="${"true"}">\u2192</span></a></div>

        <div class="${"mt-6 grid grid-cols-1 gap-y-6 sm:grid-cols-2 sm:grid-rows-2 sm:gap-x-6 lg:gap-8"}"><div class="${"group aspect-w-2 aspect-h-1 rounded-lg overflow-hidden sm:aspect-h-1 sm:aspect-w-1 sm:row-span-2"}"><img src="${"https://tailwindui.com/img/ecommerce-images/home-page-03-featured-category.jpg"}" alt="${"Two models wearing women's black cotton crewneck tee and off-white cotton crewneck tee."}" class="${"object-center object-cover group-hover:opacity-75"}">
                <div aria-hidden="${"true"}" class="${"bg-gradient-to-b from-transparent to-black opacity-50"}"></div>
                <div class="${"p-6 flex items-end"}"><div><h3 class="${"font-semibold text-white"}"><a href="${"#"}"><span class="${"absolute inset-0"}"></span>
                                New Arrivals
                            </a></h3>
                        <p aria-hidden="${"true"}" class="${"mt-1 text-sm text-white"}">Shop now
                        </p></div></div></div>
            <div class="${"group aspect-w-2 aspect-h-1 rounded-lg overflow-hidden sm:relative sm:aspect-none sm:h-full"}"><img src="${"https://tailwindui.com/img/ecommerce-images/home-page-03-category-01.jpg"}" alt="${"Wooden shelf with gray and olive drab green baseball caps, next to wooden clothes hanger with sweaters."}" class="${"object-center object-cover group-hover:opacity-75 sm:absolute sm:inset-0 sm:w-full sm:h-full"}">
                <div aria-hidden="${"true"}" class="${"bg-gradient-to-b from-transparent to-black opacity-50 sm:absolute sm:inset-0"}"></div>
                <div class="${"p-6 flex items-end sm:absolute sm:inset-0"}"><div><h3 class="${"font-semibold text-white"}"><a href="${"#"}"><span class="${"absolute inset-0"}"></span>
                                Accessories
                            </a></h3>
                        <p aria-hidden="${"true"}" class="${"mt-1 text-sm text-white"}">Shop now
                        </p></div></div></div>
            <div class="${"group aspect-w-2 aspect-h-1 rounded-lg overflow-hidden sm:relative sm:aspect-none sm:h-full"}"><img src="${"https://tailwindui.com/img/ecommerce-images/home-page-03-category-02.jpg"}" alt="${"Walnut desk organizer set with white modular trays, next to porcelain mug on wooden desk."}" class="${"object-center object-cover group-hover:opacity-75 sm:absolute sm:inset-0 sm:w-full sm:h-full"}">
                <div aria-hidden="${"true"}" class="${"bg-gradient-to-b from-transparent to-black opacity-50 sm:absolute sm:inset-0"}"></div>
                <div class="${"p-6 flex items-end sm:absolute sm:inset-0"}"><div><h3 class="${"font-semibold text-white"}"><a href="${"#"}"><span class="${"absolute inset-0"}"></span>
                                Workspace
                            </a></h3>
                        <p aria-hidden="${"true"}" class="${"mt-1 text-sm text-white"}">Shop now
                        </p></div></div></div></div>

        <div class="${"mt-6 sm:hidden"}"><a href="${"#"}" class="${"block text-sm font-semibold text-indigo-600 hover:text-indigo-500"}">Browse all categories<span aria-hidden="${"true"}">\u2192</span></a></div></div></div>`;
});
var ProductsBar = create_ssr_component(($$result, $$props, $$bindings, slots) => {
  return `
<div class="${"bg-white"}"><div class="${"max-w-2xl mx-auto py-8 px-4 sm:py-8 sm:px-6 lg:max-w-7xl lg:px-8"}"><div class="${"md:flex md:items-center md:justify-between"}"><h2 class="${"text-2xl font-extrabold tracking-tight text-gray-900"}">Skateboards populaires
            </h2>
            <a href="${"#"}" class="${"hidden text-sm font-medium text-indigo-600 hover:text-indigo-500 md:block"}">Voir tout<span aria-hidden="${"true"}">\u2192</span></a></div>

        <div class="${"mt-6 grid grid-cols-2 gap-x-4 gap-y-10 sm:gap-x-6 md:grid-cols-4 md:gap-y-0 lg:gap-x-8"}"><div class="${"group relative"}"><div class="${"w-full h-56 bg-gray-200 rounded-md overflow-hidden group-hover:opacity-75 lg:h-72 xl:h-80"}"><img src="${"https://tailwindui.com/img/ecommerce-images/home-page-04-trending-product-02.jpg"}" alt="${"Hand stitched, orange leather long wallet."}" class="${"w-full h-full object-center object-cover"}"></div>
                <h3 class="${"mt-4 text-sm text-gray-700"}"><a href="${"#"}"><span class="${"absolute inset-0"}"></span>
                        Leather Long Wallet
                    </a></h3>
                <p class="${"mt-1 text-sm text-gray-500"}">Natural</p>
                <p class="${"mt-1 text-sm font-medium text-gray-900"}">$75</p></div>

            </div>

        <div class="${"mt-8 text-sm md:hidden"}"><a href="${"#"}" class="${"font-medium text-indigo-600 hover:text-indigo-500"}">Shop the collection<span aria-hidden="${"true"}">\u2192</span></a></div></div></div>`;
});
var HomeCategories = create_ssr_component(($$result, $$props, $$bindings, slots) => {
  return `
<div class="${"bg-white"}"><div class="${"py-8 sm:py-8 xl:max-w-7xl xl:mx-auto xl:px-8"}"><div class="${"px-4 sm:px-6 sm:flex sm:items-center sm:justify-between lg:px-8 xl:px-0"}"><h2 class="${"text-2xl font-extrabold tracking-tight text-gray-900"}">Shop by Category
            </h2>
            <a href="${"#"}" class="${"hidden text-sm font-semibold text-indigo-600 hover:text-indigo-500 sm:block"}">Browse all categories<span aria-hidden="${"true"}">\u2192</span></a></div>

        <div class="${"mt-4 flow-root"}"><div class="${"-my-2"}"><div class="${"box-content py-2 relative h-80 overflow-x-auto xl:overflow-visible"}"><div class="${"absolute min-w-screen-xl px-4 flex space-x-8 sm:px-6 lg:px-8 xl:relative xl:px-0 xl:space-x-0 xl:grid xl:grid-cols-5 xl:gap-x-8"}"><a href="${"#"}" class="${"relative w-56 h-80 rounded-lg p-6 flex flex-col overflow-hidden hover:opacity-75 xl:w-auto"}"><span aria-hidden="${"true"}" class="${"absolute inset-0"}"><img src="${"https://tailwindui.com/img/ecommerce-images/home-page-01-category-01.jpg"}" alt="${""}" class="${"w-full h-full object-center object-cover"}"></span>
                            <span aria-hidden="${"true"}" class="${"absolute inset-x-0 bottom-0 h-2/3 bg-gradient-to-t from-gray-800 opacity-50"}"></span>
                            <span class="${"relative mt-auto text-center text-xl font-bold text-white"}">New Arrivals</span></a>

                        <a href="${"#"}" class="${"relative w-56 h-80 rounded-lg p-6 flex flex-col overflow-hidden hover:opacity-75 xl:w-auto"}"><span aria-hidden="${"true"}" class="${"absolute inset-0"}"><img src="${"https://tailwindui.com/img/ecommerce-images/home-page-01-category-02.jpg"}" alt="${""}" class="${"w-full h-full object-center object-cover"}"></span>
                            <span aria-hidden="${"true"}" class="${"absolute inset-x-0 bottom-0 h-2/3 bg-gradient-to-t from-gray-800 opacity-50"}"></span>
                            <span class="${"relative mt-auto text-center text-xl font-bold text-white"}">Productivity</span></a>

                        <a href="${"#"}" class="${"relative w-56 h-80 rounded-lg p-6 flex flex-col overflow-hidden hover:opacity-75 xl:w-auto"}"><span aria-hidden="${"true"}" class="${"absolute inset-0"}"><img src="${"https://tailwindui.com/img/ecommerce-images/home-page-01-category-04.jpg"}" alt="${""}" class="${"w-full h-full object-center object-cover"}"></span>
                            <span aria-hidden="${"true"}" class="${"absolute inset-x-0 bottom-0 h-2/3 bg-gradient-to-t from-gray-800 opacity-50"}"></span>
                            <span class="${"relative mt-auto text-center text-xl font-bold text-white"}">Workspace</span></a>

                        <a href="${"#"}" class="${"relative w-56 h-80 rounded-lg p-6 flex flex-col overflow-hidden hover:opacity-75 xl:w-auto"}"><span aria-hidden="${"true"}" class="${"absolute inset-0"}"><img src="${"https://tailwindui.com/img/ecommerce-images/home-page-01-category-05.jpg"}" alt="${""}" class="${"w-full h-full object-center object-cover"}"></span>
                            <span aria-hidden="${"true"}" class="${"absolute inset-x-0 bottom-0 h-2/3 bg-gradient-to-t from-gray-800 opacity-50"}"></span>
                            <span class="${"relative mt-auto text-center text-xl font-bold text-white"}">Accessories</span></a>

                        <a href="${"#"}" class="${"relative w-56 h-80 rounded-lg p-6 flex flex-col overflow-hidden hover:opacity-75 xl:w-auto"}"><span aria-hidden="${"true"}" class="${"absolute inset-0"}"><img src="${"https://tailwindui.com/img/ecommerce-images/home-page-01-category-03.jpg"}" alt="${""}" class="${"w-full h-full object-center object-cover"}"></span>
                            <span aria-hidden="${"true"}" class="${"absolute inset-x-0 bottom-0 h-2/3 bg-gradient-to-t from-gray-800 opacity-50"}"></span>
                            <span class="${"relative mt-auto text-center text-xl font-bold text-white"}">Sale</span></a></div></div></div></div>

        <div class="${"mt-6 px-4 sm:hidden"}"><a href="${"#"}" class="${"block text-sm font-semibold text-indigo-600 hover:text-indigo-500"}">Browse all categories<span aria-hidden="${"true"}">\u2192</span></a></div></div></div>`;
});
var Reassurance = create_ssr_component(($$result, $$props, $$bindings, slots) => {
  return `
<div class="${"bg-white"}"><div class="${"max-w-7xl mx-auto px-4 sm:px-6 lg:px-8"}"><div class="${"bg-gray-50 rounded-2xl px-6 sm:p-4"}"><div class="${"max-w-xl mx-auto lg:max-w-none"}"><div class="${"mt-12 max-w-sm mx-auto grid grid-cols-1 gap-y-10 gap-x-8 sm:max-w-none lg:grid-cols-3"}"><div class="${"text-center sm:flex sm:text-left lg:block lg:text-center"}"><div class="${"sm:flex-shrink-0"}"><div class="${"flow-root"}"><img class="${"w-16 h-16 mx-auto"}" src="${"https://tailwindui.com/img/ecommerce/icons/icon-shipping-simple.svg"}" alt="${""}"></div></div>
                        <div class="${"mt-3 sm:mt-0 sm:ml-6 lg:mt-6 lg:ml-0"}"><h3 class="${"text-sm font-medium text-gray-900"}">Free shipping
                            </h3>
                            <p class="${"mt-2 text-sm text-gray-500"}">It&#39;s not actually free we just price it
                                into the products. Someone&#39;s paying for it,
                                and it&#39;s not us.
                            </p></div></div>

                    <div class="${"text-center sm:flex sm:text-left lg:block lg:text-center"}"><div class="${"sm:flex-shrink-0"}"><div class="${"flow-root"}"><img class="${"w-16 h-16 mx-auto"}" src="${"https://tailwindui.com/img/ecommerce/icons/icon-warranty-simple.svg"}" alt="${""}"></div></div>
                        <div class="${"mt-3 sm:mt-0 sm:ml-6 lg:mt-6 lg:ml-0"}"><h3 class="${"text-sm font-medium text-gray-900"}">10-year warranty
                            </h3>
                            <p class="${"mt-2 text-sm text-gray-500"}">If it breaks in the first 10 years we&#39;ll
                                replace it. After that you&#39;re on your own
                                though.
                            </p></div></div>

                    <div class="${"text-center sm:flex sm:text-left lg:block lg:text-center"}"><div class="${"sm:flex-shrink-0"}"><div class="${"flow-root"}"><img class="${"w-16 h-16 mx-auto"}" src="${"https://tailwindui.com/img/ecommerce/icons/icon-exchange-simple.svg"}" alt="${""}"></div></div>
                        <div class="${"mt-3 sm:mt-0 sm:ml-6 lg:mt-6 lg:ml-0"}"><h3 class="${"text-sm font-medium text-gray-900"}">Exchanges
                            </h3>
                            <p class="${"mt-2 text-sm text-gray-500"}">If you don&#39;t like it, trade it to one of
                                your friends for something of theirs. Don&#39;t
                                send it here though.
                            </p></div></div></div></div></div></div></div>`;
});
var Routes = create_ssr_component(($$result, $$props, $$bindings, slots) => {
  return `${validate_component(Showcase, "Showcase").$$render($$result, {}, {}, {})}
${validate_component(ProductsBar, "ProductsBar").$$render($$result, {}, {}, {})}
${validate_component(ProductsBar, "ProductsBar").$$render($$result, {}, {}, {})}
${validate_component(HomeCategories, "HomeCategories").$$render($$result, {}, {}, {})}
${validate_component(Reassurance, "Reassurance").$$render($$result, {}, {}, {})}`;
});
var index = /* @__PURE__ */ Object.freeze({
  __proto__: null,
  [Symbol.toStringTag]: "Module",
  "default": Routes
});
var Sign_in = create_ssr_component(($$result, $$props, $$bindings, slots) => {
  return `
<div class="${"max-w-7xl mx-auto bg-white flex"}"><div class="${"hidden lg:block relative w-0 flex-1"}"><img class="${"absolute inset-0 h-full w-full object-cover"}" src="${"https://images.unsplash.com/photo-1579189842650-c2a3b321c1e9?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8"}" alt="${""}"></div>
    <div class="${"flex-1 flex flex-col justify-center py-12 px-4 sm:px-6 lg:flex-none lg:px-20 xl:px-24"}"><div class="${"mx-auto w-full max-w-sm lg:w-96"}"><div><h2 class="${"mt-6 text-3xl font-extrabold text-gray-900"}">Connexion
                </h2></div>

            <div class="${"mt-8"}"><div><div><p class="${"text-sm font-medium text-gray-700"}">Sign in with
                        </p>

                        <div class="${"mt-1 grid grid-cols-1 gap-3"}"><div><a href="${"#"}" class="${"w-full inline-flex justify-center py-2 px-4 border border-gray-300 rounded-md shadow-sm bg-white text-sm font-medium text-gray-500 hover:bg-gray-50"}"><span class="${"sr-only"}">Sign in with Facebook</span>
                                    <svg class="${"w-5 h-5"}" fill="${"currentColor"}" viewBox="${"0 0 20 20"}" aria-hidden="${"true"}"><path fill-rule="${"evenodd"}" d="${"M20 10c0-5.523-4.477-10-10-10S0 4.477 0 10c0 4.991 3.657 9.128 8.438 9.878v-6.987h-2.54V10h2.54V7.797c0-2.506 1.492-3.89 3.777-3.89 1.094 0 2.238.195 2.238.195v2.46h-1.26c-1.243 0-1.63.771-1.63 1.562V10h2.773l-.443 2.89h-2.33v6.988C16.343 19.128 20 14.991 20 10z"}" clip-rule="${"evenodd"}"></path></svg></a></div></div></div>

                    <div class="${"mt-6 relative"}"><div class="${"absolute inset-0 flex items-center"}" aria-hidden="${"true"}"><div class="${"w-full border-t border-gray-300"}"></div></div>
                        <div class="${"relative flex justify-center text-sm"}"><span class="${"px-2 bg-white text-gray-500"}">Or continue with
                            </span></div></div></div>

                <div class="${"mt-6"}"><form action="${"#"}" method="${"POST"}" class="${"space-y-6"}"><div><label for="${"email"}" class="${"block text-sm font-medium text-gray-700"}">Email address
                            </label>
                            <div class="${"mt-1"}"><input id="${"email"}" name="${"email"}" type="${"email"}" autocomplete="${"email"}" required class="${"appearance-none block w-full px-3 py-2 border border-gray-300 rounded-md shadow-sm placeholder-gray-400 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"}"></div></div>

                        <div class="${"space-y-1"}"><label for="${"password"}" class="${"block text-sm font-medium text-gray-700"}">Password
                            </label>
                            <div class="${"mt-1"}"><input id="${"password"}" name="${"password"}" type="${"password"}" autocomplete="${"current-password"}" required class="${"appearance-none block w-full px-3 py-2 border border-gray-300 rounded-md shadow-sm placeholder-gray-400 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"}"></div></div>

                        <div class="${"flex items-center justify-between"}"><div class="${"flex items-center"}"><input id="${"remember-me"}" name="${"remember-me"}" type="${"checkbox"}" class="${"h-4 w-4 text-indigo-600 focus:ring-indigo-500 border-gray-300 rounded"}">
                                <label for="${"remember-me"}" class="${"ml-2 block text-sm text-gray-900"}">Remember me
                                </label></div>

                            <div class="${"text-sm"}"><a href="${"#"}" class="${"font-medium text-indigo-600 hover:text-indigo-500"}">Forgot your password?
                                </a></div></div>

                        <div><button type="${"submit"}" class="${"w-full flex justify-center py-2 px-4 border border-transparent rounded-md shadow-sm text-sm font-medium text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"}">Sign in
                            </button></div></form></div></div></div></div></div>`;
});
var signIn = /* @__PURE__ */ Object.freeze({
  __proto__: null,
  [Symbol.toStringTag]: "Module",
  "default": Sign_in
});

// .svelte-kit/netlify/entry.js
init();
async function handler(event) {
  const { path, httpMethod, headers, rawQuery, body, isBase64Encoded } = event;
  const query = new URLSearchParams(rawQuery);
  const encoding = isBase64Encoded ? "base64" : headers["content-encoding"] || "utf-8";
  const rawBody = typeof body === "string" ? Buffer.from(body, encoding) : body;
  const rendered = await render({
    method: httpMethod,
    headers,
    path,
    query,
    rawBody
  });
  if (rendered) {
    return {
      isBase64Encoded: false,
      statusCode: rendered.status,
      ...splitHeaders(rendered.headers),
      body: rendered.body
    };
  }
  return {
    statusCode: 404,
    body: "Not found"
  };
}
function splitHeaders(headers) {
  const h = {};
  const m = {};
  for (const key in headers) {
    const value = headers[key];
    const target = Array.isArray(value) ? m : h;
    target[key] = value;
  }
  return {
    headers: h,
    multiValueHeaders: m
  };
}
// Annotate the CommonJS export names for ESM import in node:
0 && (module.exports = {
  handler
});
