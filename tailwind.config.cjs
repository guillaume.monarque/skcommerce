const colors = require("tailwindcss/colors")
const config = {
    mode: "jit",
    purge: ["./src/**/*.{html,js,svelte,ts}"],
    theme: {
        extend: {
            colors: {
                topBar: colors.black,
            },
        },
    },
    plugins: [
        require("@tailwindcss/forms"),
        require("@tailwindcss/aspect-ratio"),
    ],
}

module.exports = config
