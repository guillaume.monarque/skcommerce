import preprocess from "svelte-preprocess"
import adapter from "@sveltejs/adapter-netlify"
/** @type {import('@sveltejs/kit').Config} */
const config = {
    kit: {
        // hydrate the <div id="svelte"> element in src/app.html
        adapter: adapter(),
        target: "#svelte",
        vite: {
            server: {
                hmr: {
                    protocol: "wss", //gitignore
                    host: "24678-yellow-goldfish-s0wqxhzc.ws-eu16.gitpod.io", //gitignore
                    clientPort: "443", //gitignore
                },
            },
        },
    },

    preprocess: [
        preprocess({
            postcss: true,
        }),
    ],
}

export default config
