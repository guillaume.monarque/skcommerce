export async function get() {
    let headerText = "Livraison en point relais offerte"
    let tabs = [
        {
            label: "Skateboards",
            value: 1,
            items: [
                {
                    header: null,
                    categories: [
                        { label: "Skate complets", link: "/trucks" },
                        { label: "Skate enfants", link: "/trucks" },
                        { label: "Cruisers", link: "/trucks" },
                    ],
                },
                {
                    header: "Matériel",
                    categories: [
                        { label: "Decks", link: "/trucks" },
                        { label: "Trucks", link: "/trucks" },
                        { label: "Roues", link: "/roues" },
                        { label: "Roulements", link: "/roues" },
                        { label: "Visserie", link: "/roues" },
                        { label: "Grips", link: "/grips" },
                        { label: "Bushings", link: "/grips" },
                        { label: "Pads", link: "/grips" },
                    ],
                },
                {
                    header: "Accessoires",
                    categories: [
                        { label: "Tools", link: "/trucks" },
                        { label: "Bushings", link: "/trucks" },
                        { label: "Goodies", link: "/roues" },
                    ],
                },
            ],
            cards: [
                {
                    label: "Nouveautés skateboard",
                    action: "Voir tout",
                    url: "google.fr",
                    image: "https://images.pexels.com/photos/1870376/pexels-photo-1870376.jpeg",
                    alt: "Some alt",
                },
                {
                    label: "Nouveautés skateboard",
                    action: "Voir tout",
                    url: "google.fr",
                    image: "https://images.pexels.com/photos/1870376/pexels-photo-1870376.jpeg",
                    alt: "Some alt",
                },
            ],
        },
        {
            label: "Longboards",
            value: 2,
            items: [
                {
                    header: null,
                    categories: [
                        { label: "Skate complets", link: "/trucks" },
                        { label: "Skate enfants", link: "/trucks" },
                        { label: "Cruisers", link: "/trucks" },
                    ],
                },
                {
                    header: "Matériel",
                    categories: [
                        { label: "Decks", link: "/trucks" },
                        { label: "Trucks", link: "/trucks" },
                        { label: "Roues", link: "/roues" },
                        { label: "Roulements", link: "/roues" },
                        { label: "Visserie", link: "/roues" },
                        { label: "Grips", link: "/grips" },
                        { label: "Bushings", link: "/grips" },
                        { label: "Pads", link: "/grips" },
                    ],
                },
                {
                    header: "Accessoires",
                    categories: [
                        { label: "Tools", link: "/trucks" },
                        { label: "Bushings", link: "/trucks" },
                        { label: "Goodies", link: "/roues" },
                    ],
                },
            ],
            cards: [
                {
                    label: "Nouveautés skateboard",
                    action: "Voir tout",
                    link: "google.fr",
                    image: "https://images.pexels.com/photos/1870376/pexels-photo-1870376.jpeg",
                    alt: "Some alt",
                },
                {
                    label: "Nouveautés skateboard",
                    action: "Voir tout",
                    link: "google.fr",
                    image: "https://images.pexels.com/photos/1870376/pexels-photo-1870376.jpeg",
                    alt: "Some alt",
                },
            ],
        },
    ]

    return {
        body: { tabs: tabs, headerText: headerText },
    }
}
